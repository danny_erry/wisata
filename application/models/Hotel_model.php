<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Hotel_model extends CI_Model {
	
	function get_hotel(){
		$q = 'select a.*, min(b.harga) as harga,c.hotel_image_src
				from hotel a
				left join kamar_hotel b on b.hotel_id = a.hotel_id
				left join hotel_image c on c.hotel_id = a.hotel_id
				group by a.hotel_id';
		return $this->db->query($q)->result();
	}
	
	function get_hotel_paging($halaman=''){
		$q = 'select a.*, min(b.harga) as harga,c.hotel_image_src
				from hotel a
				left join kamar_hotel b on b.hotel_id = a.hotel_id
				left join hotel_image c on c.hotel_id = a.hotel_id
				group by a.hotel_id limit '.(($halaman-1)*9).',9';
		return $this->db->query($q)->result();
	}
	
	function get_hotel_by_id($id){
		$data = $this->db->get_where('hotel', array('hotel_id' => $id));
		return $data->row();
	}
	
	function get_kamar_hotel_by_id($id){
		$data = $this->db->get_where('kamar_hotel', array('hotel_id' => $id));
		return $data->result();
	}
	
	function get_foto_hotel_by_id($id){
		$data = $this->db->get_where('hotel_image', array('hotel_id' => $id));
		return $data->result();
	}

}