<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home_model extends CI_Model {

	function get_slider(){
		$this->db->select('*');
		$this->db->from('slider');
		return $query = $this->db->get()->result();
	}
	
	function get_event(){
		$this->db->select('*');
		$this->db->from('event');
		return $query = $this->db->get()->result();
	}
	
	function get_tempat_wisata(){
		$this->db->select('a.*,b.image_src');
		$this->db->from('tourist_atraction a');
		$this->db->join('ta_image b','a.ta_id = b.ta_id','left');
		$this->db->group_by('b.ta_id');
		$this->db->order_by('a.ta_id','decs');
		$this->db->limit(4);
		return $query = $this->db->get()->result();
	}
	
	function get_event_by_id($id){
		$this->db->select('a.*');
		$this->db->from('event a');
		$this->db->where('a.event_id',$id);
		return $query = $this->db->get()->row();
	}

}