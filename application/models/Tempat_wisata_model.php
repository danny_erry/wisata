<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tempat_wisata_model extends CI_Model {

	
	function get_tempat_wisata(){
		$this->db->select('a.*,b.image_src,group_concat(c.type_id) as jenis');
		$this->db->from('tourist_atraction a');
		$this->db->join('ta_image b','a.ta_id = b.ta_id','left');
		$this->db->join('ta_type c','a.ta_id = c.ta_id','left');
		$this->db->group_by('a.ta_id');
		$this->db->order_by('a.ta_id','decs');
		return $query = $this->db->get()->result();
	}
	
	function get_tempat_wisata_paging($halaman='',$jenis=''){
		$this->db->select('a.*,b.image_src,group_concat(c.type_id) as jenis');
		$this->db->from('tourist_atraction a');
		$this->db->join('ta_image b','a.ta_id = b.ta_id','left');
		$this->db->join('ta_type c','a.ta_id = c.ta_id','left');
		if($jenis != '' && $jenis != 'all'){
			$this->db->where('c.type_id',$jenis);
		}
		if($halaman != ''){
			$this->db->limit(9,(($halaman-1)*9));
		}
		$this->db->group_by('a.ta_id');
		$this->db->order_by('a.ta_id','decs');
		//$this->db->get();
		//var_dump($this->db->last_query(),$halaman); die();
		return $query = $this->db->get()->result();
	}
	
	function get_type(){
		$this->db->select('*');
		$this->db->from('type_tempat_wisata');
		return $this->db->get()->result();
	}
	
	function get_tempat_wisata_by_id($id){
		$this->db->select('a.*');
		$this->db->from('tourist_atraction a');
		$this->db->where('a.ta_id',$id);
		return $query = $this->db->get()->row();
	}
	
	function get_ta_image($id){
		$this->db->select('a.*');
		$this->db->from('ta_image a');
		$this->db->where('a.ta_id',$id);
		return $query = $this->db->get()->result();
	}
	
	function get_ta_facility($id){
		$this->db->select('a.*,b.*');
		$this->db->from('ta_facility a');
		$this->db->join('facility_tempat_wisata b','a.ta_facility=b.ta_facility');
		$this->db->where('a.ta_id',$id);
		return $query = $this->db->get()->result();
	}

}