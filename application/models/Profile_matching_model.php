<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile_matching_model extends CI_Model {
	
	function get_type(){
		$this->db->select('*');
		$this->db->from('type_tempat_wisata');
		return $this->db->get()->result();
	}
	
	function get_facility(){
		$this->db->select('*');
		$this->db->from('facility_tempat_wisata');
		return $this->db->get()->result();
	}
	
	function get_hotel($harga){
		$this->db->select('a.*');
		$this->db->from('hotel as a');
		$this->db->join('kamar_hotel as b','a.hotel_id=b.hotel_id');
		$this->db->where('b.harga <= ',$harga);
		$this->db->group_by('a.hotel_id');
		return $this->db->get()->result();
	}
	
	function get_pilih_jenis($jenis){
		$this->db->distinct();
		$this->db->select('ta_id');
		$this->db->from('ta_type');
		foreach($jenis as $j){
			$this->db->or_where('type_id',$j);
		}
		return $this->db->get()->result();
	}
	
	function get_pilih_fasilitas($fasilitas){
		$this->db->distinct();
		$this->db->select('ta_id');
		$this->db->from('ta_facility');
		foreach($fasilitas as $j){
			$this->db->or_where('ta_facility',$j);
		}
		return $this->db->get()->result();
	}
	
	function get_wisata_terpilih($pilih){
		$this->db->select('a.*, group_concat(distinct b.ta_facility) as fasilitas, group_concat(distinct c.type_id) as type');
		$this->db->from('tourist_atraction a ');
		$this->db->join('ta_facility b','b.ta_id = a.ta_id');
		$this->db->join('ta_type c','c.ta_id = a.ta_id');
		$this->db->where_in('a.ta_id',$pilih);
		$this->db->group_by('a.ta_id');
		return $this->db->get()->result();
		/*
		$q = 'select a.*, group_concat(distinct b.ta_facility) as fasilitas, group_concat(distinct c.type_id) as type 
			from tourist_atraction a 
			join ta_facility b on b.ta_id = a.ta_id
			join ta_type c on c.ta_id = a.ta_id
			group by a.ta_id
			';
			return $this->db->query($q);
		*/		
	}
}