<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hotel extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
		$this->load->model('hotel_model');
        
    } 
	
	function _view($template = '',$param = ''){
		$this->load->view('front end/core/header',$param);
		$this->load->view($template,$param);
		$this->load->view('front end/core/footer');
	} 
	 
	public function index()
	{
		$data['page'] = 'Hotel';
		$data['hotel'] = $this->hotel_model->get_hotel();
		$this->_view('front end/hotel',$data);
	}
	
	function get_hotel(){
		$halaman = $this->input->post('halaman');
		$data = $this->hotel_model->get_hotel_paging($halaman);
		foreach($data as $x=>$p){
			$data[$x]->hotel_desc = substr($p->hotel_desc, 0, strrpos(substr($p->hotel_desc, 0, 100), ' '));
			$data[$x]->harga = number_format($p->harga);
		}
		echo json_encode($data);
	}

	
	public function detail_hotel($id='')
	{
		$hotel = $this->hotel_model->get_hotel_by_id($id);
		$data['page'] = 'Detail Hotel '.$hotel->hotel_name;
		$data['hotel'] = $hotel;
		$data['kamar'] = $this->hotel_model->get_kamar_hotel_by_id($id);
		$data['foto'] = $this->hotel_model->get_foto_hotel_by_id($id);
		$this->_view('front end/detail_hotel',$data);
	}
}
