<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tempat_wisata extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model('tempat_wisata_model');
    } 
	
	function _view($template = '',$param = ''){
		$this->load->view('front end/core/header',$param);
		$this->load->view($template,$param);
		$this->load->view('front end/core/footer');
	} 
	 
	public function index()
	{
		$data['page'] = 'Tempat Wisata';
		$data['jenis'] = $this->tempat_wisata_model->get_type();
		$data['tempat_wisata'] = $this->tempat_wisata_model->get_tempat_wisata();
		$this->_view('front end/tempat_wisata',$data);
	}
	
	function detail($id=''){
		$tempat_wisata = $this->tempat_wisata_model->get_tempat_wisata_by_id($id);
		$data['tempat_wisata'] = $tempat_wisata;
		$data['page'] = 'Detail tempat Wisata '.$tempat_wisata->ta_name;
		$data['gambar'] = $this->tempat_wisata_model->get_ta_image($id);
		$data['fasilitas'] = $this->tempat_wisata_model->get_ta_facility($id);
		$this->_view('front end/detail_wisata',$data);
	}
	
	function get_tempat_wisata(){
		$halaman = $this->input->post('halaman');
		$jenis = $this->input->post('jenis');
		$data = $this->tempat_wisata_model->get_tempat_wisata_paging($halaman,$jenis);
		foreach($data as $x=>$p){
			$data[$x]->ta_desc = substr($p->ta_desc, 0, strrpos(substr($p->ta_desc, 0, 100), ' '));
			$data[$x]->ticket_price = number_format($p->ticket_price);
		}
		echo json_encode($data);
	}
	
	function hitung_semua(){
		$halaman = '';
		$jenis = $this->input->post('jenis');
		$data = $this->tempat_wisata_model->get_tempat_wisata_paging($halaman,$jenis);
		echo ceil(count($data)/9);
	}
}
