<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model('home_model');
    } 
	
	function _view($template = '',$param = ''){
		$this->load->view('front end/core/header',$param);
		$this->load->view($template,$param);
		$this->load->view('front end/core/footer');
	} 
	 
	public function index()
	{
		$data['page'] = '';
		$data['slider'] = $this->home_model->get_slider();
		$data['event'] = $this->home_model->get_event();
		$data['tempat_wisata'] = $this->home_model->get_tempat_wisata();
		
		$this->_view('front end/home',$data);
	}
	
	function detail($id){
		$event = $this->home_model->get_event_by_id($id);
		$data['page'] = 'Detail Event '.$event->event_name;
		$data['event'] = $event;
		$this->_view('front end/detail_event',$data);
	}
	
}
