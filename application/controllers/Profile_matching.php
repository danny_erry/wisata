<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile_matching extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
		$this->load->model('profile_matching_model');
        
    }

	function hitung_uang($jumlah){
		if($jumlah <= 100000 ){
			return 1;
		}else if(($jumlah > 101000) && ($jumlah <= 200000)){
			return 2;
		}
		else if(($jumlah > 201000) && ($jumlah <= 300000)){
			return 3;
		}else if(($jumlah > 301000) && ($jumlah <= 400000)){
			return 4;
		}else{
			return 5;
		}		
	}
	
	function hitung_jarak($param){
		$param = $param/1000;
		if($param <= 5){
			return 1;
		}else if($param > 5 && $param <= 10 ){
			return 2;
		}else if($param > 10 && $param <= 15 ){
			return 3;
		}else if($param > 15 && $param <= 20 ){
			return 4;
		}else{
			return 5;
		}
	}
	
	function convert($param){
		$convert[0] = 6;
		$convert[1] = 5.5;
		$convert[-1] = 5;
		$convert[2] = 4.5;
		$convert[-2] = 4;
		$convert[3] = 3.5;
		$convert[-3] = 3;
		$convert[4] = 2.5;
		$convert[-4] = 2;
		$convert[5] = 1.5;
		$convert[-5] = 1;
		if (array_key_exists($param,$convert)){
			return $convert[$param];
		}else{
			return 1;
		}
		
	}
	
	function get_jarak($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000)
	{
		 
		$latFrom = deg2rad($latitudeFrom);
		$lonFrom = deg2rad($longitudeFrom);
		$latTo = deg2rad($latitudeTo);
		$lonTo = deg2rad($longitudeTo);

		$lonDelta = $lonTo - $lonFrom;
		$a = pow(cos($latTo) * sin($lonDelta), 2) +
			pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
		$b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);

		$angle = atan2(sqrt($a), $b);
		return ($angle * $earthRadius)/1000;
	}
	
	function _view($template = '',$param = ''){
		$this->load->view('front end/core/header',$param);
		$this->load->view($template,$param);
		$this->load->view('front end/core/footer');
	} 
	 
	public function index()
	{
		$data['type'] = $this->profile_matching_model->get_type();
		$data['facility'] = $this->profile_matching_model->get_facility();
		$data['page'] = 'Profile Matching';
		$this->_view('front end/profile_matching',$data);
	}
	
	function sorting($myArray){
		/*$myArray[0] = array(
			'id' => 1,
			'nilai' => 4
		);
		$myArray[1] = array(
			'id' => 2,
			'nilai' => 2
		);
		$myArray[2] = array(
			'id' => 3,
			'nilai' => 3
		);*/
		function cmp($a, $b) {
			return $a["nilai"] - $b["nilai"];
		}
		usort($myArray, "cmp");
		
		return $myArray;
	}
	
	function hitung(){
		
		$lamakunjung = $this->input->post('lamakunjung');
		$tempatinap = $this->input->post('tempatinap');
		$fasilitasPendukung = $this->input->post('fasilitas_pendukung');
		$tipePendukung = $this->input->post('tipe_pendukung');
		$budget = str_replace(',','',$this->input->post('budget'));
		$anggaran = explode(',',$this->input->post('anggaran'));
		$jenis = $this->input->post('jenis');
		$fasilitas = $this->input->post('fasilitas');
		$alamatinap = $this->input->post('alamatinap');
		
		
		
		
		$uanghotel = 0;
		$uangtiket = 0;
		$uangmakan = 0;
		$uangtransport = 0;
		if(count($anggaran) > 1){
			if(!$alamatinap){
				$uanghotel = 0.2*$budget;
			}
			
			if(in_array("tiket",$anggaran)){
				$uangtiket = 0.15*$budget;
			}
			
			if(in_array("makan",$anggaran)){
				$uangmakan = 0.15*$budget;
			}
			
			if(in_array("transport",$anggaran)){
				$uangtransport = 0.25*$budget;
			}
			$budget=$budget-($uanghotel+$uangtiket+$uangmakan+$uangtransport);
			$jatahWisata = $uangtiket / ($lamakunjung*2);
		}else{
			$jatahWisata = $budget / ($lamakunjung*2);
		}
		$nilaiAnggaran = $this->hitung_uang($jatahWisata);
		
		$pilihJenis =  $this->profile_matching_model->get_pilih_jenis($jenis);
		$pilihFasilitas =  $this->profile_matching_model->get_pilih_fasilitas($fasilitas);
		
		
		$wisataJenis =  array();
		foreach($pilihJenis as $j){
			$wisataJenis[] = $j->ta_id;
		}
		
		$wisataFasilitas =  array();
		foreach($pilihFasilitas as $f){
			$wisataFasilitas[] = $f->ta_id;
		}
		
		$arrayWisata = array_intersect($wisataJenis,$wisataFasilitas);
		if(count($arrayWisata)==0){
			$arrayWisata = $wisataJenis;
		}
		//
		$arrayWisata[] = 1;
		//
		if(!empty($arrayWisata)){
			$wisataTerpilih =  $this->profile_matching_model->get_wisata_terpilih($arrayWisata);
			foreach($wisataTerpilih as $t){
				$wisata[] = $t->ta_id;
				$wisataName[] = $t->ta_name;
				$wisataLat[] = $t->ta_lat;
				$wisataLong[] = $t->ta_long;
				$wisataHarga[] = $t->ticket_price;
				$jenisWisata = explode(',',$t->type);
				$fasilitasWisata = explode(',',$t->fasilitas);
				if(count($jenis) > count($jenisWisata)){
					$gapJenis[] = count($jenis) - count(array_intersect($jenis,$jenisWisata));
				}else{
					$gapJenis[] = count(array_intersect($jenis,$jenisWisata)) - count($jenisWisata);
				}
				
				if(count($fasilitas) > count($fasilitasWisata)){
					$gapFasilitas[] = count($fasilitas) - count(array_intersect($fasilitas,$fasilitasWisata));
				}else{
					$gapFasilitas[] = count(array_intersect($fasilitas,$fasilitasWisata)) - count($fasilitasWisata);
				}

				if($tempatinap=='Yes'){
					$tempatinap = explode($tempatinap);
					$jarak = $this->get_jarak($tempatinap[0],$tempatinap[1],$t->ta_lat,$t->ta_long);
					$gapJarak[] = $this->hitung_jarak($jarak);
					$secondaryJarak[] = $t->jarak_point;
				}				
				
				$gapFasilitasPendukung[] = $fasilitasPendukung - $t->type_point;
				$gapFasilitasPendukung2[] = $t->facility_point2;
				$gapTypePendukung[] = $tipePendukung - $t->facility_point;
				$gapAnggaran[]  = $nilaiAnggaran - ($this->hitung_uang($t->ticket_price));
			}
			
			for($i=0;$i<count($gapJenis);$i++){
				$nilaiJenis[$i] = (0.6 *  $this->convert($gapJenis[$i])) + (0.4 * $this->convert($gapTypePendukung[$i]));
				$nilaiFasilitas[$i] = (0.6 *  $this->convert($gapFasilitas[$i])) + (0.4 * ($this->convert($gapFasilitasPendukung[$i])+$this->convert($gapFasilitasPendukung2[$i]))/2);
				$nilaiBiaya[$i] = (0.6 *  $this->convert($gapAnggaran[$i])) + (0.4 * $this->convert(count($anggaran)));
				
				if($tempatinap=='Yes'){
					$nilaiJarak[$i] = (0.6 *  $this->convert($gapJarak[$i])) + (0.4 * $this->convert($secondaryJarak));
					$nilaiRangking[$i] = (0.3 * $nilaiBiaya[$i]) + (0.2 * $nilaiFasilitas[$i]) + (0.2 * $nilaiJenis[$i]) + (0.3 * $nilaiJarak[$i]);
				}else{
					$nilaiRangking[$i] = (0.4 * $nilaiBiaya[$i]) + (0.3 * $nilaiFasilitas[$i]) + (0.3 * $nilaiJenis[$i]);
				}
				$arrayjadi[] = array(
					'id' => $wisata[$i],
					'nilai' => $nilaiRangking[$i],
					'nama' => $wisataName[$i],
					'lat' => $wisataLat[$i],
					'long' => $wisataLong[$i],
					'harga' => $wisataHarga[$i]
				);
				
			}
			$arrayUrut = $this->sorting($arrayjadi);
			$wisataUtama = array();
			$wisataCadangan = array();
			$hitung = 0;
			for($i=0;$i<count($arrayUrut);$i++){
				$hitung = $hitung + $arrayUrut[$i]['harga'];
				if($hitung <= $uangtiket){
					$wisataUtama[] =  $arrayUrut[$i];
				}else{
					$wisataCadangan[] =  $arrayUrut[$i];
					if(count($wisataCadangan)==3){
						break;
					}
				}
			}
			$data['kirim'] = $wisataUtama;
			$data['cadangan'] = $wisataCadangan;
			$data['page'] = 'Hasil Sistem';
			
			if($tempatinap=='No'){
				if(count($anggaran)==1){
					$data['hotel'] = $this->profile_matching_model->get_hotel($budget);
				}else{
					$data['hotel'] = $this->profile_matching_model->get_hotel($uanghotel/$lamakunjung);
				}
				
			}
			$this->_view('front end/hasil_hitung',$data);
		}
		
		/*
		$arrayjadi[0] = array(
			'id' => 1,
			'nilai' => 4,
			'nama' => 'Satu',
			'lat' => '-7.9828666',
			'long' => '112.631497'
		);
		$arrayjadi[1] = array(
			'id' => 2,
			'nilai' => 2,
			'nama' => 'Dua',
			'lat' => '-7.9658969',
			'long' => '112.6052842'
		);
		$arrayjadi[2] = array(
			'id' => 3,
			'nilai' => 3,
			'nama' => 'Tiga',
			'lat' => '-7.9737834',
			'long' => '112.6381596'
		);
		
		$arrayCadangan[0] = array(
			'id' => 1,
			'nilai' => 4,
			'nama' => 'Boheimian',
			'lat' => '-7.9747917',
			'long' => '112.6146412'
		);
		$arrayCadangan[1] = array(
			'id' => 2,
			'nilai' => 2,
			'nama' => 'Bogor',
			'lat' => '-7.9601495',
			'long' => '112.619359'
		);
		$data['kirim'] = $this->sorting($arrayjadi);
		$data['cadangan'] = $arrayCadangan;
		$data['page'] = 'Hasil Sistem';
		$data['hotel'] = $this->profile_matching_model->get_hotel(500000000);
		$this->_view('front end/hasil_hitung',$data);
		*/
	}
	
}
