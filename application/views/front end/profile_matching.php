<style>
	.pemisah1{
		padding:5px;
		height:75px;
		border-bottom:1px solid black;
	}
	
	.pemisah{
		padding:5px;
		border-bottom:1px solid black;
	}
	
	.pemisah2{
		padding:5px;
		height:125px;
		border-bottom:1px solid black;
	}
</style>
			<main class="content">
				<div class="fullwidth-block">
					<div class="container">
						<div class="row">
							<form action="<?=base_url('profile_matching/hitung');?>" method="POST" id="myform">
								<div class="col-md-12 wow fadeInLeft">
									<div class="row">
										<div class="col-md-12 col-xs-12 pemisah">
											<div class="col-md-6 col-xs-12">
												Lama Berkunjung(Hari) <br/>
												<input type="number" min=1 class="form-control" name="lamakunjung" oninput="tempatInap()" style="width:50%">
											</div>
											<div class="col-md-6 col-xs-12" id="tempatInap" style="display:none">
												Apakah Anda Sudah Memiliki Tempat Untuk Menginap ? <br/>
												<div class="radio">
													<label><input type="radio" value="Yes" name="tempatinap">Ya</label>
													<label><input type="radio" value="No" name="tempatinap">Tidak</label>
												</div>
												<div id="alamatinap" style="display:none">
													Masukkan alamat tempat anda menginap
													<input type="text" id="inapadd" class="form-control" name="alamatinap" onchange="myIP()">
													<input type="hidden" name="langlong" id="langlong" class="form-control" placeholder="Lang / Long" readonly>
												</div>
											</div>
										</div>
										
										<div class="col-md-12 col-xs-12 pemisah" id="dua" style="display:none;">
											<div class="col-md-6 col-xs-12">
												Budget Yang Disediakan<span id="budget-hotel">(Include Hotel)</span> <br/>
												<input type="text" class="form-control" name="budget" style="width:50%" oninput="this.value=addCommas(this.value);">
											</div>
											<div class="col-md-6 col-xs-12" id="macam-anggaran" style="display:none">
												Anggaran Biaya Tersebut Digunakan Untuk Apa Saja ? <br/>
												<label><input type="radio" value="tiket" name="anggaran">Untuk tiket masuk saja</label>
												<label><input type="radio" value="tiket,transport" name="anggaran">Untuk tiket masuk tempat wisata, dan uang transportasi</label>
												<label><input type="radio" value="tiket,transport,makan" name="anggaran">Untuk tiket masuk tempat wisata, uang transportasi dan makan</label>
											</div>
										</div>
										<div class="col-md-12 col-xs-12 pemisah" id="tiga" style="display:none;">
											<div class="col-md-6 col-xs-12">
												Jenis Lokasi Wisata Yang Diinginkan <br/>
												<?php foreach($type as $t){ ?>
													<label class="checkbox-inline"><input type="checkbox" name="jenis[]" value="<?=$t->id_type?>"><?=$t->type_name?></label><br/>
												<?php } ?>
											</div>
											<div class="col-md-6 col-xs-12" id="fasilitasdiv" style="display:none">
												Fasilitas Yang Diinginkan <br/>
												<?php foreach($facility as $f){ ?>
													<label class="checkbox-inline"><input type="checkbox" value="<?=$f->ta_facility?>" name="fasilitas[]"><?=$f->facility_name?></label><br/>
												<?php } ?>
											</div>
										</div>
										<div class="col-md-12 col-xs-12 pemisah" id="empat" style="display:none;">
											<div class="col-md-6 col-xs-12">
												Fasilitas Pendukung <br/>
												<label><input type="radio" value="0" name="fasilitas_pendukung">Belum Ada Fasilitas</label><br/>
												<label><input type="radio" value="1" name="fasilitas_pendukung">Fasilitas menanggung asuransi</label><br/>
												<label><input type="radio" value="2" name="fasilitas_pendukung">Fasilitas menanggung asuransi dan guide</label><br/>
												<label><input type="radio" value="3" name="fasilitas_pendukung">Fasilitas menanggung asuransi, guide, dan rest area</label><br/>
											</div>
											<div class="col-md-6 col-xs-12" id="fasilitasdiv">
												Data Pendukung <br/>
												<label><input type="radio" value="1" name="tipe_pendukung">Wisata yang baru dikenal khalayak umum</label><br/>
												<label><input type="radio" value="2" name="tipe_pendukung">Wisata yang baru dikenal khalayak umum, dan media lokal</label><br/>
												<label><input type="radio" value="3" name="tipe_pendukung">Wisata yang baru dikenal khalayak umum dan media nasional</label><br/>
												<label><input type="radio" value="4" name="tipe_pendukung">Wisata yang baru dikenal khalayak umum, media nasional, dan media internasional</label>
											</div>
										</div>
										
									</div>
									<button type="submit" id="proses" class="btn success" style="display:none;">Proses</button>
								</div>
							</form>	
						</div>
					</div>
				</div>
			</main>
	<script>
		$( document ).ready(function() {
			$('#myform').on('keyup keypress', function(e) {
			  var keyCode = e.keyCode || e.which;
			  if (keyCode === 13) { 
				e.preventDefault();
				return false;
			  }
			});
		});	
	</script>
	<script>
		function tempatInap(){
			$('#tempatInap').show();
		}
		
		$('input:radio[name="tempatinap"]').change(function(){
			if ($(this).val() == 'Yes') {
				$('#alamatinap').show();
				$('#budget-hotel').hide();
			}else{
				$('#budget-hotel').show();
				$('#alamatinap').hide();
			}
			$('#dua').show();
		});
		
		$('input:radio[name="anggaran"]').change(function(){
			$('#tiga').show();
		});
		
		$('input:checkbox[name="fasilitas[]"]').change(function(){
			$('#empat').show();
			$('#proses').show();
		});
		
		$('input:checkbox[name="jenis[]"]').change(function(){
			$('#fasilitasdiv').show();
		});
	</script>
	<script>
		function myIP(){
			var geocoder =  new google.maps.Geocoder();
				geocoder.geocode( { 'address': $('#inapadd').val()}, function(results, status) {
				if (status == google.maps.GeocoderStatus.OK) {
					$('#langlong').val(results[0].geometry.location.lat() + "," +results[0].geometry.location.lng()); 
				} else {
					$('#langlong').val("Something got wrong " + status);
				}
			});
		}
	</script>
	
	<script type="text/javascript">
	function addCommas(s)
	{
		$('#macam-anggaran').show();
		s = s.replace(/,/g, "");
			
			if(isNaN(s)){
				return s;
			}else{
				if(s.length < 3 ){
					return s;
				}else{
					
					var i = s.length % 3;
					var parts = i ? [ s.substr( 0, i ) ] : [];
					for( ; i < s.length ; i += 3 )
					{
						parts.push( s.substr( i, 3 ) );
					}
					var jadi = '';
					for(i=0;i<parts.length;i++){
						if(jadi == ''){
							jadi = jadi+parts[i];
						}else{
							jadi = jadi+','+parts[i];
						}
					}
					//console.log(parts);
					return jadi;
				}
			}
	}
  </script>
	