			<main class="content">
				<div class="fullwidth-block">
					<div class="container">
						<input type="hidden" id="halaman" value="1">
						<input type="hidden" id="jumlah-maksimal" value="<?=ceil(count($hotel)/9)?>">
						<div class="data-hotel">
							<?php 
								foreach($hotel as $x=>$h){
									if($x<=8){
							?>
										<div class="filterable-item">
											<article class="offer-item">
												<figure class="featured-image">
													<a href="<?=base_url('hotel/detail_hotel/').$h->hotel_id?>"><img src="<?=base_url('admin/asset/images/hotel/').$h->hotel_image_src ?>" alt=""></a>
												</figure>
												<h2 class="entry-title"><a href="<?=base_url('hotel/detail_hotel/').$h->hotel_id?>"><?=$h->hotel_name;?></a></h2>
												<p><?=substr($h->hotel_desc, 0, strrpos(substr($h->hotel_desc, 0, 100), ' '));?></p>
												<div class="price">
													<strong>Rp. <?=number_format($h->harga)?></strong>
													<small>/hari</small>
												</div>
											</article>
										</div>
							<?php 
									}
								}
							?>
						</div>

						<div class="pagination wow fadeInUp col-md-12">
							<a href="#" class="page-numbers" id="pertama" onclick="first()"><<</a>
							<a href="#" class="page-numbers" id="sebelumnya" onclick="sebelumnya()"><</a>
							Show page
							<a href="#" class="page-numbers current"><span id="sekarang"></span></a>
							From <span id="banyak"></span>
							<a href="#" class="page-numbers" id="selanjutnya" onclick="selanjutnya()">></a>
							<a href="#" class="page-numbers" id="terakir" onclick="last()">>></a>
						</div>

					</div>

				</div>
			</main>
			<script>
				function first(){
					$('#halaman').val(1);
					paging();
					var halaman = $('#halaman').val();
					$('#sekarang').text(halaman);
					atur();
				}
				
				function last(){
					var jumlah = $('#jumlah-maksimal').val();
					$('#halaman').val(jumlah);
					paging();
					var halaman = $('#halaman').val();
					$('#sekarang').text(halaman);
					atur();
				}
				
				function sebelumnya(){
					var halaman = parseInt($('#halaman').val())-1;
					$('#halaman').val(halaman);
					paging();
					var halaman = $('#halaman').val();
					$('#sekarang').text(halaman);
					atur();
				}
				
				function selanjutnya(){
					var halaman = parseInt($('#halaman').val())+1;
					$('#halaman').val(halaman);
					paging();
					var halaman = $('#halaman').val();
					$('#sekarang').text(halaman);
					atur();
				}
				
			</script>
			<script>
				window.base_url = <?php echo json_encode(base_url()); ?>;
				function paging(){
					$('.data-hotel').empty();
					var dataString = 'halaman='+$('#halaman').val();
					$.ajax({
						type:'POST',
						data:dataString,
						url:'<?= base_url("hotel/get_hotel") ?>',
						success:function(kembali) {
							var data = JSON.parse(kembali);
							for(var i=0;i<data.length;i++){
								var apa = '<div class="filterable-item"><article class="offer-item"><figure class="featured-image">'
											+'<a href="<?=base_url('hotel/detail_hotel/')?>'+data[i].hotel_id+'"><img src="<?=base_url("admin/asset/images/hotel/")?>'+data[i].hotel_image_src+'" alt=""></a>'
											+'</figure><h2 class="entry-title"><a href="<?=base_url('hotel/detail_hotel/')?>'+data[i].hotel_id+'">'+data[i].hotel_name+'</a></h2>'
											+'<p>'+data[i].hotel_desc+'</p><div class="price">'
											+'<strong>Rp. '+data[i].harga+'</strong><small>/hari</small></div></article></div>';
								$('.data-hotel').append($(apa));		
							}
						},error: function(xhr, status, error) {
						  var err = eval("(" + xhr.responseText + ")");
						  console.log(err);
						}
					});				
				}
			</script>
			<script>
				$(document).ready(function () {
					atur();
					var halaman = $('#halaman').val();
					$('#sekarang').text(halaman);
				});
			</script>
			<script>
				function atur(){
					var jumlah = $('#jumlah-maksimal').val();
					var halaman = $('#halaman').val();
					if(jumlah==halaman){
						
						$('#selanjutnya').hide();
						$('#terakir').hide();
					}else{
						$('#selanjutnya').show();
						$('#terakir').show();
					}
					if(halaman==1){
						$('#pertama').hide();
						$('#sebelumnya').hide();
					}else{
						$('#pertama').show();
						$('#sebelumnya').show();
					}
					
					$('#banyak').text(jumlah);
				}
			</script>