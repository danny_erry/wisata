<style>
	#marquee {
		margin: 50px;
		width: 300px;
		height: 400px;
		
		overflow: hidden; /* <-- so we can see what happens */
	}
	#marquee > div {
		padding: 1em;
	}
</style>			
			<main class="content">
				<div class="slider">
					<ul class="slides">
						<?php
							foreach($slider as $s){
						?>
								<li data-background="<?=base_url('admin/asset/images/slider/').$s->slider_src?>">
									<div class="container">
										<div class="slide-caption col-md-4">
											<h2 class="slide-title"><?=$s->slider_caption?></h2>
											<p><?=$s->slider_desc?></p>
										</div>
									</div>
								</li>
						<?php 
							} 
						?>
					</ul>
					<div class="flexslider-controls">
						<div class="container">
							<ol class="flex-control-nav">
								<?php
									foreach($slider as $x=>$s){
								?>
										<li><a><?=$x+1?></a></li>
								<?php
									}
								?>
							</ol>
						</div>
					</div>
				</div>

				

				<div class="fullwidth-block offers-section" data-bg-color="#f1f1f1">
					<div class="container" >
						<h2 class="section-title">Tempat Wisata Terbaru</h2>
						<div class="row" style="width:80%;float:left;">
						
							<?php
								foreach($tempat_wisata as $t){
							?>
									<div class="col-md-3 col-sm-6 col-xs-12">
										<article class="offer wow bounceIn">
											<figure class="featured-image"><img src="<?=base_url('admin/asset/images/wisata/').$t->image_src?>" alt=""></figure>
											<h2 class="entry-title"><a href="<?=base_url('tempat_wisata/detail/').$t->ta_id?>"><?=$t->ta_name?></a></h2>
											<p><?=substr($t->ta_desc, 0, strrpos(substr($t->ta_desc, 0, 100), ' '));?></p>
											<a href="<?=base_url('tempat_wisata/detail/').$t->ta_id?>" class="button">See details</a>
										</article>
									</div>
							<?php
								}
							?>
						</div>
						<div class="row" style="width:20%;float:right;height: 400px;margin-top:-50px;">
							<div id='marquee'> 
								<?php
									foreach($event as $x=>$e){
								?>
										<div id='div<?=$x+1?>'>
											<article class="offer">
												<figure class="featured-image"><img src="<?=base_url('admin/asset/images/event/').$e->event_image?>" alt=""></figure>
												<h2 class="entry-title"><a href="<?=base_url('home/detail/').$e->event_id?>"><?=$e->event_name?></a></h2>
												<p><?=substr($e->event_desc, 0, strrpos(substr($t->ta_desc, 0, 100), ' '));?></p>
											</article>
										</div>
								<?php
									}
								?>
								
							</div>
						
						</div>
					</div>
				</div>

				 <!-- .content -->

			