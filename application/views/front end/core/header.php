<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1">
		
		<title><?=$page?></title>
		<!-- Loading third party fonts -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,600,700" rel="stylesheet" type="text/css">
		<link href="<?=base_url('asset/')?>fonts/font-awesome.min.css" rel="stylesheet" type="text/css">
		<!-- Loading main css file -->
		<link rel="stylesheet" href="<?=base_url('asset/')?>css/animate.min.css">
		<link rel="stylesheet" href="<?=base_url('asset/')?>style.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>
		
		
		<!--[if lt IE 9]>
		<script src="js/ie-support/html5.js"></script>
		<script src="js/ie-support/respond.js"></script>
		<![endif]-->

	</head>

	<body>
		
		<div id="site-content">
			
			<header class="site-header wow fadeInDown">
				<div class="container">
					<div class="header-content">
						<div class="branding">
							<img src="<?=base_url('asset/')?>images/logo.png" alt="Company Name" class="logo">
							<h1 class="site-title"><a href="index.html">STIKI Malang</a></h1>
							<small class="site-description">Jl. Raya Tidar 100, Malang</small>
						</div>
						
						<nav class="main-navigation">
							<button type="button" class="menu-toggle"><i class="fa fa-bars"></i></button>
							<ul class="menu">
								<li class="menu-item current-menu-item"><a href="<?=base_url('home')?>">Home</a></li>
								<li class="menu-item"><a href="<?=base_url('hotel')?>">Hotel</a></li>
								<li class="menu-item tempat-wisata"><a href="<?=base_url('tempat_wisata')?>">Tempat Wisata</a></li>
								<li class="menu-item"><a href="<?=base_url('contact_us')?>">Contact</a></li>
							</ul>
						</nav>
						
						<div class="social-links">
							<a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
							<a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
							<a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
							<a href="#" class="pinterest"><i class="fa fa-pinterest"></i></a>
						</div>
					</div>
					<nav class="breadcrumbs">
						<a href="<?=base_url()?>">Home</a> &rarr;
						<span><?=$page?></span>
					</nav>
				</div>
			</header> <!-- .site-header -->