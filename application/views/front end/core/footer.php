<footer class="site-footer wow fadeInUp">
				<div class="footer-top">
					<div class="container">
						<div class="row">
							<div class="col-md-3 col-sm-6">
								<div class="widget">
									<h3 class="widget-title">About us</h3>
									<p>Website ini bertujuan untuk membantu calon wisatawan untuk menentukan tempat wisata dan tempat penginapan di kota Malang.</p>
								</div>
							</div>
							<div class="col-md-3 col-sm-6">
								<div class="widget">
									&nbsp;
								</div>
							</div>
							<div class="col-md-3 col-sm-6">
								<div class="widget">
									&nbsp;
								</div>
							</div>
							<div class="col-md-3 col-sm-6">
								<div class="widget widget-customer-info">
									<h3 class="widget-title">Customer Service</h3>
									<img src="<?=base_url('asset/')?>dummy/footer-customer-service.jpg" alt="" class="pull-left">
									<div class="cs-info">
										<p>Silahkan hubungi kami pada</p>
										<p>+62 857-5561-6222 <br> bimasurya45@gmail.com</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="footer-bottom">
					<div class="container">
						<div class="branding pull-left">
							<img src="<?=base_url('asset/')?>images/logo-footer.png" alt="Company Name" class="logo">
							<h1 class="site-title"><a href="index.html">STIKI Malang</a></h1>
							<small class="site-description">Jl. Raya Tidar 100, Malang</small>
						</div>

						
					</div>
				</div>
				<div class="colophon">
					<div class="container">
						<p class="copy">Copyright 2017 STIKI</p>
					</div>
				</div>
			</footer> <!-- .site-footer -->

		</div> <!-- #site-content -->
		<script src="<?=base_url('asset/js/min/plugins-min.js')?>"></script>
		<script src="<?=base_url('asset/js/min/app-min.js')?>"></script>
		
	</body>

</html>
<script>
	$(document).ready(function () {
		var x=0;
		var z =0;
		$('.menu a').each(function (index) {
			if (this.href.trim() == window.location) {
				$(this).closest('li').addClass("current-menu-item");
				localStorage.setItem('activeMenu', z);
				x++;
			}else{
				$(this).closest('li').removeClass("current-menu-item");
			}
			z++;
		});
		if(x==0){
			var y = localStorage.getItem('activeMenu');
			var i = 0;
			$('.menu a').each(function (index) {
				if(i==y){
					$(this).closest('li').addClass("current-menu-item");
				}
				i++;
			});
			
		}
	});
</script>
<script>
	(function($) {

    var methods = {
        init: function(options) {
            this.children(':first').stop();
            this.marquee('play');
        },
        play: function() {
            var marquee = this,
                pixelsPerSecond = 100,
                firstChild = this.children(':first'),
                totalHeight = 0,
                difference,
                duration;
            
            // Find the total height of the children by adding each child's height:
            this.children().each(function(index, element) {
                totalHeight += $(element).innerHeight();
            });
            
            // The distance the divs have to travel to reach -1 * totalHeight:
            difference = totalHeight + parseInt(firstChild.css('margin-top'), 10);
            
            // The duration of the animation needed to get the correct speed:
            duration = (difference/pixelsPerSecond) * 1000;
            
            // Animate the first child's margin-top to -1 * totalHeight:
            firstChild.animate(
                { 'margin-top': -1 * totalHeight },
                duration,
                'linear',
                function() {
                    // Move the first child back down (below the container):
                    firstChild.css('margin-top', marquee.innerHeight());
                    // Restart whole process... :)
                    marquee.marquee('play');
                }
            );
        },
        pause: function() {
            this.children(':first').stop();
        }
    };

    $.fn.marquee = function(method) {

        // Method calling logic
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.marquee');
        }

    };

})(jQuery);

var marquee = $('#marquee');

marquee.marquee();

marquee.hover(function() {
    marquee.marquee('pause');
}, function() {
    marquee.marquee('play');    
});</script>
