			<style>
				.dipilih{
					background:#19bdce;
				}
			</style>
			<main class="content">
				<div class="fullwidth-block">
					<div class="container">
						<div class="filter-links ">
							<a href="<?=base_url('profile_matching')?>" class="dipilih">Bantuan</a>
						</div>
						<div class="filter-links filterable-nav">
							<!--<select class="mobile-filter" id="dd">
								<option value="*">Show all</option>
								<?php
									foreach($jenis as $j){
								?>
									<option value=".<?=$j->id_type;?>"><?=$j->type_name;?></option>
								<?php 
									}
								?>	
							</select>
							-->
							<a href="#" class=" current wow fadeInRight filtering dipilih" onclick="getValue(this,'all')">Show all</a>
							<?php
								foreach($jenis as $j){
							?>
								<a href="#" class="wow fadeInRight filtering"  onclick="getValue(this,'<?=$j->id_type;?>')"><?=$j->type_name;?></a>
							<?php 
								}
							?>
						</div>
						<input type="hidden" id="jenis" value="">
						<input type="hidden" id="halaman" value="1">
						<input type="hidden" id="jumlah-maksimal" value="<?=ceil(count($tempat_wisata)/9)?>">
						<div class="data-wisata">
							<?php
								foreach($tempat_wisata as $jumlah=>$p){
									if($jumlah<=8){
							?>
							<div class="filterable-item">
								<article class="offer-item">
									<figure class="featured-image">
										<a href="<?=base_url('tempat_wisata/detail/').$p->ta_id?>"><img src="<?=base_url('admin/asset/images/wisata/').$p->image_src?>" alt=""></a>
									</figure>
									<h2 class="entry-title"><a href="<?=base_url('tempat_wisata/detail/').$p->ta_id?>"><?=$p->ta_name?></a></h2>
									<p><?=substr($p->ta_desc, 0, strrpos(substr($p->ta_desc, 0, 100), ' '));?></p>
									<div class="price">
										<strong>Rp. <?=number_format($p->ticket_price);?></strong>
									</div>
								</article>
							</div>
							<?php
									}
								}
							?>
							
						</div>

						<div class="pagination wow fadeInUp col-md-12">
							<a href="#" class="page-numbers" id="pertama" onclick="first()"><<</a>
							<a href="#" class="page-numbers" id="sebelumnya" onclick="sebelumnya()"><</a>
							Show page
							<a href="#" class="page-numbers current"><span id="sekarang"></span></a>
							From <span id="banyak"></span>
							<a href="#" class="page-numbers" id="selanjutnya" onclick="selanjutnya()">></a>
							<a href="#" class="page-numbers" id="terakir" onclick="last()">>></a>
						</div>

					</div>

				</div>

				
			</main>
			<script>
				function first(){
					$('#halaman').val(1);
					paging();
					var halaman = $('#halaman').val();
					$('#sekarang').text(halaman);
					atur();
				}
				
				function last(){
					var jumlah = $('#jumlah-maksimal').val();
					$('#halaman').val(jumlah);
					paging();
					var halaman = $('#halaman').val();
					$('#sekarang').text(halaman);
					atur();
				}
				
				function sebelumnya(){
					var halaman = parseInt($('#halaman').val())-1;
					$('#halaman').val(halaman);
					paging();
					var halaman = $('#halaman').val();
					$('#sekarang').text(halaman);
					atur();
				}
				
				function selanjutnya(){
					var halaman = parseInt($('#halaman').val())+1;
					$('#halaman').val(halaman);
					paging();
					var halaman = $('#halaman').val();
					$('#sekarang').text(halaman);
					atur();
				}
				function getValue(data,param){
					$('.filtering').removeClass('dipilih');
					$(data).addClass('dipilih');
					$('#jenis').val(param);
					$('#halaman').val(1);
					$('#sekarang').text(1);
					paging();
					
				}
			</script>
			
			<script>
				window.base_url = <?php echo json_encode(base_url()); ?>;
				function paging(){
					$('.data-wisata').empty();
					var dataString = 'halaman='+$('#halaman').val()+'&jenis='+$('#jenis').val();
					$.ajax({
						type:'POST',
						data:dataString,
						url:'<?= base_url("tempat_wisata/get_tempat_wisata") ?>',
						success:function(kembali) {
							var data = JSON.parse(kembali);
							for(var i=0;i<data.length;i++){
								var apa = '<div class="filterable-item">'
										+'<article class="offer-item">'
										+'<figure class="featured-image">'
										+'<a href="<?=base_url('tempat_wisata/detail/')?>'+data[i].ta_id+'"><img src="<?=base_url('admin/asset/images/wisata/')?>'+data[i].image_src+'" alt=""></a>'
										+'</figure>'
										+'<h2 class="entry-title"><a href="<?=base_url('tempat_wisata/detail/')?>'+data[i].ta_id+'">'+data[i].ta_name+'</a></h2>'
										+'<p>'+data[i].ta_desc+'</p>'
										+'<div class="price">'
										+'<strong>Rp. '+data[i].ticket_price+'</strong>'
										+'</div>'
										+'</article>'
										+'</div>';
								$('.data-wisata').append($(apa));		
							}
						},error: function(xhr, status, error) {
						  var err = eval("(" + xhr.responseText + ")");
						  console.log(err);
						}
					});
					
					$.ajax({
						type:'POST',
						data:dataString,
						url:'<?= base_url("tempat_wisata/hitung_semua") ?>',
						success:function(data) {
							$('#banyak').text(data);
							$('#jumlah-maksimal').val(data);
							atur();
						},error: function(xhr, status, error) {
						  var err = eval("(" + xhr.responseText + ")");
						  console.log(err);
						}
					});
				}
			</script>
			
			<script>
				$(document).ready(function () {
					atur();
					var halaman = $('#halaman').val();
					$('#sekarang').text(halaman);
				});
			</script>
			<script>
				function atur(){
					var jumlah = $('#jumlah-maksimal').val();
					var halaman = $('#halaman').val();
					if(jumlah==halaman){
						
						$('#selanjutnya').hide();
						$('#terakir').hide();
					}else{
						$('#selanjutnya').show();
						$('#terakir').show();
					}
					if(halaman==1){
						$('#pertama').hide();
						$('#sebelumnya').hide();
					}else{
						$('#pertama').show();
						$('#sebelumnya').show();
					}
					
					$('#banyak').text(jumlah);
				}
			</script>