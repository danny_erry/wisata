			<main class="content">
				<div class="fullwidth-block">
					<div class="container">
						<div class="row">
							<div class="col-md-8 wow fadeInLeft">
								<h2 class="section-title"><?=$event->event_name?></h2>
								<p><?=$event->event_desc?></p> 
							</div>
							<div class="col-md-3 col-md-push-1 wow fadeInRight">
								
									<img src="<?=base_url('admin/asset/images/event/').$event->event_image?>" alt="" style="height:250px;width:250px;">
								
							</div>
						</div>

					</div>

				</div>

				
			</main> 