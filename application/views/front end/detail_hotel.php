			<main class="content">
				<div class="fullwidth-block">
					<div class="container">
						<div class="row">
							<div class="col-md-8 wow fadeInLeft">
								<h2 class="section-title"><?=$hotel->hotel_name?></h2>
								<p><?=$hotel->hotel_desc;?></p> 

								<div class="row">
									<div class="col-md-3 col-xs-6">
										<h4>Harga</h4>
										<ul class="list-arrow">
											<?php
												foreach($kamar as $k){
											?>
												<li><?=$k->jenis_kamar_hotel?> - Rp. <?=number_format($k->harga)?></li>
											<?php
												}
											?>
										</ul>
									</div>
									
								</div>
							</div>
							<div class="col-md-3 col-md-push-1 wow fadeInRight">
								<?php
									foreach($foto as $f){
								?>
										<img src="<?=base_url('admin/asset/images/hotel/').$f->hotel_image_src ;?>" alt="" style="height:250px;width:250px">
								<?php 
									}
								?>
							</div>
						</div>

					</div>

				</div>

				
			</main> 