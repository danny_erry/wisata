			<main class="content">
				<div class="fullwidth-block">
					<div class="container">
						<div class="row">
							<div class="col-md-8 wow fadeInLeft">
								<h2 class="section-title"><?=$tempat_wisata->ta_name?></h2>
								<p><?=$tempat_wisata->ta_desc?></p> 

								

								<div class="row">
									<div class="col-md-3 col-xs-6">
										<h4>Harga</h4>
										<ul class="list-arrow">
											<li>
												<strong>Rp. <?=number_format($tempat_wisata->ticket_price);?></strong>
											</li>
										</ul>
									</div>
									<div class="col-md-3 col-xs-6">
										<h4>Fasilitas</h4>
										<ul class="list-arrow">
											<?php 
												foreach($fasilitas as $f){
											?>
												<li><?=$f->facility_name?></li>
											<?php
												}
											?>
										</ul>
									</div>
									
								</div>
							</div>
							<div class="col-md-3 col-md-push-1 wow fadeInRight">
								<?php
									foreach($gambar as $g){
								?>
									<img src="<?=base_url('admin/asset/images/wisata/').$g->image_src?>" alt="" style="height:250px;width:250px;">
								<?php
									}
								?>
							</div>
						</div>

					</div>

				</div>

				
			</main> 