			
			<style>
				.dipilih{
					background:#19bdce;
				}
			</style>
			<link rel="stylesheet" href="<?=base_url('asset/leaflet/dist/leaflet.css') ?>" />
			<script src="<?=base_url('asset/leaflet/dist/leaflet.js');?>"></script>
			
			<!-----  Menampilkan Navigasi panah 4 arah  ------>
			<script src="<?=base_url('asset/pancontrol/L.Control.Pan.js')?>" ></script>
			
			<!-----  sumber peta menggunakan Google Map  ------>
			<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDieAQ8Zk7oTOOskC_i_8TQFH8gBaVBEzo" type="text/javascript"></script>
			<script src="<?=base_url('asset/js/Google.js')?>"></script>
			<script src="<?=base_url('asset/js/maplabel-compiled.js')?>"></script>
			
			
			<!-----  sumber peta : Bing Maps  ------>
			<script src="<?=base_url('asset/js/Bing.js')?>"></script>
			<script>
				$( document ).ready(function() {
					$("input:checkbox").prop('checked',true);
				});
			</script>
			<script>
				window.base_url = <?php echo json_encode(base_url()); ?>;
			</script>
			<script>
				var hotelInti = [];
				var LabelhotelInti = [];
				var hotelCadangan = [];
				var LabelhotelCadangan = [];
				var wisataInti = [];
				var LabelwisataInti = [];
				var wisataCadangan = [];
				var LabelwisataCadangan = [];
				var map;
				function init() {
					var bounds = new google.maps.LatLngBounds();
					var i;
					var myLatlng = new google.maps.LatLng(-7.9771124,112.6318789);
					var myOptions = {
					  zoom: 15,
					  center: myLatlng,
					  mapTypeId: google.maps.MapTypeId.ROADMAP
					};
					map = new google.maps.Map(document.getElementById('map'), myOptions);
					<?php 
					for($i=0;$i<count($kirim);$i++){ ?>
						<?php if(!empty($kirim[$i]['lat']) && !empty($kirim[$i]['long']) && $kirim[$i]['lat'] != '' && $kirim[$i]['long'] != '' ){ ?>
							
							var mapLabel = new MapLabel({
								text: '<?=$kirim[$i]['nama']?>',
								position: new google.maps.LatLng(<?php echo $kirim[$i]['lat'] ?>, <?php echo $kirim[$i]['long'] ?>),
								map: map,
								fontSize: 18,
								fontColor: '#009900',
								align: 'midle'
							});
							mapLabel.set('position', new google.maps.LatLng(<?php echo $kirim[$i]['lat'] ?>, <?php echo $kirim[$i]['long'] ?>));
							
							bounds.extend(new google.maps.LatLng(<?=$kirim[$i]['lat'];?>, <?=$kirim[$i]['long'];?>));
							
							var marker = new google.maps.Marker({
								position: new google.maps.LatLng(<?php echo $kirim[$i]['lat'] ?>, <?php echo $kirim[$i]['long'] ?>),
								map: map,
								// icon: icon,
								title: '<?=$kirim[$i]['nama']?>',
								url: "<?=base_url()."tempat_wisata/detail/".$kirim[$i]['id'] ?>",
							});
							wisataInti.push(marker);
							LabelwisataInti.push(mapLabel);
							
							google.maps.event.addListener(marker, 'click', function() {
								window.location.href = this.url;
							});
							
								<?php
						}
							
					}
					?>
					<?php 
					for($i=0;$i<count($cadangan);$i++){ ?>
						<?php if(!empty($cadangan[$i]['lat']) && !empty($cadangan[$i]['long']) && $cadangan[$i]['lat'] != '' && $cadangan[$i]['long'] != '' ){ ?>
							
							var mapLabel = new MapLabel({
								text: '<?=$cadangan[$i]['nama']?>',
								position: new google.maps.LatLng(<?php echo $cadangan[$i]['lat'] ?>, <?php echo $cadangan[$i]['long'] ?>),
								map: map,
								fontSize: 18,
								fontColor: '#004c00',
								align: 'midle'
							});
							mapLabel.set('position', new google.maps.LatLng(<?php echo $cadangan[$i]['lat'] ?>, <?php echo $cadangan[$i]['long'] ?>));
							
							bounds.extend(new google.maps.LatLng(<?=$cadangan[$i]['lat'];?>, <?=$cadangan[$i]['long'];?>));
							
							var marker = new google.maps.Marker({
								position: new google.maps.LatLng(<?php echo $cadangan[$i]['lat'] ?>, <?php echo $cadangan[$i]['long'] ?>),
								map: map,
								// icon: icon,
								title: '<?=$cadangan[$i]['nama']?>',
								url: "<?=base_url()."tempat_wisata/detail/".$cadangan[$i]['id'] ?>",
							});
							wisataCadangan.push(marker);							
							LabelwisataCadangan.push(mapLabel);							
							
							google.maps.event.addListener(marker, 'click', function() {
								window.location.href = this.url;
							});
							
								<?php
						}
							
					}
					?>
						
						var latlng = bounds.getCenter();
						/*var marker = new google.maps.Marker({
							position: latlng,
							map: map,
							title: latlng.toString()
						});
*/
						var tengah = latlng.toString();
						var hasil = tengah.split(",");
						hasil[0] = hasil[0].replace('(','');
						hasil[1] = hasil[1].replace(')','');
						var hitung = [];
						var idHotel = [];
						var dataHotel = [];
						var namaHotel = [];
						<?php 
							if(isset($hotel)){
								foreach($hotel as $x=>$h){
						?>
									idHotel.push(<?=$h->hotel_id;?>);
									hitung.push(distance(hasil[0],hasil[1],<?=$h->hotel_lat?>,<?=$h->hotel_long?>));
									var latLong = '<?=$h->hotel_lat?>'+'~'+'<?=$h->hotel_long?>';
									dataHotel.push(latLong);
									namaHotel.push('<?=$h->hotel_name;?>');
						<?php	
								}
						?>
								for(var j = 0;j<3; j++){
									if(j==0){
										var indexMax = indexOfMax(hitung);
										hitung[indexMax] = 500000000;
										latLong = dataHotel[indexMax];
										var latLong = latLong.split("~");
										
										var latlng = new google.maps.LatLng(latLong[0], latLong[1]);
										var marker = new google.maps.Marker({
											position: latlng,
											map: map,
											title: namaHotel[indexMax],
											url: "<?=base_url()."hotel/detail_hotel/"?>"+idHotel[indexMax],
										});
										hotelInti.push(marker);
										var mapLabel = new MapLabel({
											text: namaHotel[indexMax],
											position: latlng,
											map: map,
											fontSize: 18,
											fontColor: '#926239',
											align: 'midle'
										});
										mapLabel.set('position', new google.maps.LatLng(latLong[0], latLong[1]));
										LabelhotelInti.push(mapLabel);
										
										google.maps.event.addListener(marker, 'click', function() {
											window.location.href = this.url;
										});
									}else{
										var indexMax = indexOfMax(hitung);
										hitung[indexMax] = 50000000000;
										latLong = dataHotel[indexMax];
										var latLong = latLong.split("~");
										
										var latlng = new google.maps.LatLng(latLong[0], latLong[1]);
										var marker = new google.maps.Marker({
											position: latlng,
											map: map,
											title: namaHotel[indexMax],
											url: "<?=base_url()."hotel/detail_hotel/"?>"+idHotel[indexMax],
										});
										hotelCadangan.push(marker);
										var mapLabel = new MapLabel({
											text: namaHotel[indexMax],
											position: latlng,
											map: map,
											fontSize: 18,
											fontColor: '#f6b67f',
											align: 'midle'
										});
										LabelhotelCadangan.push(mapLabel);
										mapLabel.set('position', new google.maps.LatLng(latLong[0], latLong[1]));
										
										google.maps.event.addListener(marker, 'click', function() {
											window.location.href = this.url;
										});

										
									}
								}
						<?php } ?>
				}		
				google.maps.event.addDomListener(window, 'load', init);
			
		</script>
		<script>
			function indexOfMax(arr) {
				if (arr.length === 0) {
					return -1;
				}

				var max = arr[0];
				var maxIndex = 0;

				for (var i = 1; i < arr.length; i++) {
					if (arr[i] < max) {
						maxIndex = i;
						max = arr[i];
					}
				}

				return maxIndex;
			}
		</script>
		
		<script>
			function distance(lat1, lon1, lat2, lon2) {
				var radlat1 = Math.PI * lat1/180
				var radlat2 = Math.PI * lat2/180
				var radlon1 = Math.PI * lon1/180
				var radlon2 = Math.PI * lon2/180
				var theta = lon1-lon2
				var radtheta = Math.PI * theta/180
				var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
				dist = Math.acos(dist)
				dist = dist * 180/Math.PI
				dist = dist * 60 * 1.1515;
				dist = dist * 1.609344;
				return dist;
			} 
		</script>
		<script>
			function removeMarkersHotelInti(){
				console.log(map);
				if ($('#hi').is(':checked')) {
					for(i=0; i<hotelInti.length; i++){
						hotelInti[i].setMap(map);
						LabelhotelInti[i].setMap(map);
					}
				}else{
					for(i=0; i<hotelInti.length; i++){
						hotelInti[i].setMap(null);
						LabelhotelInti[i].setMap(null);
					}
				}
			}
		</script>
		<script>
			function removeMarkersHotelCadangan(){
				console.log(map);
				if ($('#hc').is(':checked')) {
					for(i=0; i<hotelCadangan.length; i++){
						hotelCadangan[i].setMap(map);
						LabelhotelCadangan[i].setMap(map);
					}
				}else{
					for(i=0; i<hotelCadangan.length; i++){
						hotelCadangan[i].setMap(null);
						LabelhotelCadangan[i].setMap(null);
					}
				}
			}
		</script>
		<script>
			function removeMarkersWisataInti(){
				console.log(map);
				if ($('#wi').is(':checked')) {
					for(i=0; i<wisataInti.length; i++){
						wisataInti[i].setMap(map);
						LabelwisataInti[i].setMap(map);
					}
				}else{
					for(i=0; i<wisataInti.length; i++){
						wisataInti[i].setMap(null);
						LabelwisataInti[i].setMap(null);
					}
				}
			}
		</script>
		<script>
			function removeMarkersWisataCadangan(){
				console.log(map);
				if ($('#wc').is(':checked')) {
					for(i=0; i<wisataCadangan.length; i++){
						wisataCadangan[i].setMap(map);
						LabelwisataCadangan[i].setMap(map);
					}
				}else{
					for(i=0; i<wisataCadangan.length; i++){
						wisataCadangan[i].setMap(null);
						LabelwisataCadangan[i].setMap(null);
					}
				}
			}
		</script>
		
		
			<main class="content">
				<div class="fullwidth-block">
					<div class="container">
						<div class='col-xs-12 col-md-12'>
							<div id="map" style="height:600px;width:900px;float:left"></div>
							<div style="float:right">
								Legenda
								<input type="text" class="form-control" style="background-color:#009900;color:white;" value="Wisata Yang Disarankan">
								<input type="text" class="form-control" style="background-color:#004c00;color:white;font-size:10px;" value="Wisata Yang Bisa Dipertimbangkan">
								<input type="text" class="form-control" style="background-color:#926239;color:white;" value="Hotel Yang Disarankan">
								<input type="text" class="form-control" style="background-color:#f6b67f;color:white;font-size:10px;" value="Hotel Yang Bisa Dipertimbangkan">
								<label class="checkbox-inline"><input type="checkbox" id="wi" onchange="removeMarkersWisataInti()">Wisata Yang Disarankan</label><br/>
								<label class="checkbox-inline"><input type="checkbox" id="wc" onchange="removeMarkersWisataCadangan()">
									<span style="font-size:10px;">Wisata Yang Bisa Dipertimbangkan</span></label><br/>
								<label class="checkbox-inline"><input type="checkbox" id="hi" onchange="removeMarkersHotelInti()">Hotel Yang Disarankan</label><br/>
								<label class="checkbox-inline"><input type="checkbox" id="hc" onchange="removeMarkersHotelCadangan()">
									<span style="font-size:10px;">Hotel Yang Bisa Dipertimbangkan</span></label><br/>
							</div>
						</div>	

					</div>

				</div>
			</main>
			
			