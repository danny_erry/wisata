<?php

//SETTING
$MAIN_DIRECTORY_IMAGE = '/lamhot/image/';

class imageupload {
    /*
     * Upload Image 
     */

    public static function uploadImage($files) {
        $file_fullname = $files['image']['name'];
        $file_arr = explode(".", $fileFullname);
        $file_name = $file_arr[0];
        $file_ext = $file_arr[1];
        $new_file_fullname = $filename . '_original.' . $fileExt;

        move_uploaded_file($files['image']['tmp_name'], '/lamhot/image/' . $new_file_fullname);
    }

    /*
     * 
     */

    public static function uploadImageCreateClone($name, $files, $percentage, $dir) {
        $file_fullname = $files['name'];
        $file_arr = explode(".", $file_fullname);
        $file_name = $name;
        $file_ext = (explode(".", $file_fullname));
		$file_ext = $file_ext[(count($file_ext)-1)];
		$file_ext = '';
        //$original_file_fullname = $file_name . '_original.' . $file_ext;
        $new_file_fullname = $file_name ;
        $image_type = $files['type'];

//        echo '</br>';
//        if (is_dir('..\Test\lamhot\image')) {
//            echo 'is Directoery!!';
//        }  else {
//            echo 'NOooooooooooooo';
//        }
//        echo '</br>';
        

        
        //foreach ($percentage as $key => $value) {
        //copy($dir.$original_file_fullname, $dir.$new_file_fullname);
        $result = self::cloneImageResize($file_name, $files['tmp_name'], $image_type, $percentage, $dir);
        move_uploaded_file($files['tmp_name'], $dir.$new_file_fullname);
        //}
        return $result;
    }

    public static function cloneImageResize($file_name, $file_dir, $type, $size, $dir) {
        $err = false;
        $isimage = true;

        switch ($type) {
            case "image/gif": $src = imagecreatefromgif($file_dir);
                $image_type = '.gif';
                break;
            case "image/jpeg":
            case "image/pjpeg": $src = imagecreatefromjpeg($file_dir);
                $image_type = '.jpeg';
                break;
            case "image/jpg": $src = imagecreatefromjpeg($file_dir);
                $image_type = '.jpg';
                break;
            case "image/png": $src = imagecreatefrompng($file_dir);
                $image_type = '.png';
                break;
            default: $isimage = FALSE;
                break;
        }
        if ($isimage) {
            list($w, $h) = getimagesize($file_dir);
            $max = $size;
            $tw = $w;
            $th = $h;
            if ($w > $h && $max < $w) {
                $tw = $max;
                $th = $max / $w * $h;
            } elseif ($h > $w && $max < $h) {
                $tw = $max / $h * $w;
                $th = $max;
            } elseif ($max < $w) {
                $tw = $th = $max;
            }
            
            $tw = round($tw);
            $th = round($th);

            $tmp = imagecreatetruecolor($tw, $th);
            imagecopyresampled($tmp, $src, 0, 0, 0, 0, $tw, $th, $w, $h);
            imageconvolution($tmp, array(// Sharpen image
                array(-1, -1, -1),
                array(-1, 16, -1),
                array(-1, -1, -1)
                    ), 8, 0);
            imagejpeg($tmp, $dir.$file_name . $image_type);
            imagedestroy($tmp);
            imagedestroy($src);
            $err = true;
        }
        return $err;
    }

}

?>