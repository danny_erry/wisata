<style>
	.hide{
		display:none;
	}
</style>
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Tempat Wisata</li>
      </ol>
      <!-- Example DataTables Card-->
    <div class="card mb-3">
        <div class="card-header">
			<div class="card-body">
				 <?php echo form_open_multipart('wisata/do_update');?>
					
					<div class="col-lg-6 col-xs-12">
						<div class="form-group">
							<label for="exampleInputEmail1">Nama Tempat Wisata</label>
							<input type="text" class="form-control" name="nama" value="<?=$wisata->ta_name?>" placeholder="nama" >
							<input type="hidden" class="form-control" name="id" value="<?=$wisata->ta_id?>" placeholder="nama" >
						</div>
					</div>
					
					<div class="col-lg-6 col-xs-12">
						<div class="form-group">
							<label for="exampleInputEmail1">Deskripsi</label>
							<textarea class="form-control" rows="5" name="desc"><?=$wisata->ta_desc?></textarea>
						</div>
					</div>
					<div class="col-lg-6 col-xs-12">
						<div class="form-group">
							<label >Alamat</label>
							<input type="text" class="form-control" name="alamat" id="alamat" value="<?=$wisata->ta_address?>"  placeholder="alamat" onchange="myIP()">
							<input type="hidden" class="form-control" name="langlong" id='langlong' value="<?=$wisata->ta_lat?>,<?=$wisata->ta_long?>"  placeholder="alamat">
						</div>
					</div>
					<div class="col-lg-6 col-xs-12">
						<div class="form-group">
							<label for="exampleInputEmail1">Harga Tiket</label>
							<input type="text" class="form-control" name="tarif" value="<?=number_format($wisata->ticket_price)?>" oninput="this.value=addCommas(this.value);" placeholder="Tiket">
						</div>
					</div>
					<div class="col-lg-12 col-xs-12">
						<div style="width:50%;float:left">
							<div class="form-group">
								<label >Fasilitas</label><br/>
								<?php 
									foreach($fasilitas as $f){ 
										if(in_array($f->ta_facility,$ta_fasilitas)){
								?>
											<label class="checkbox-inline"><input type="checkbox" name="fasilitas[]" value="<?=$f->ta_facility?>" checked><?=$f->facility_name?></label><br/>
								<?php
										}else{ ?>
											<label class="checkbox-inline"><input type="checkbox" name="fasilitas[]" value="<?=$f->ta_facility?>"><?=$f->facility_name?></label><br/>
								<?php			
										}
									} 
								?>
							</div>
						</div>
						<div style="width:50%;float:right">
							<div class="form-group">
								<label >Fasilitas Pendukung</label><br/>
								<label><input type="radio" value="1" name="fasilitas_pendukung" <?=$wisata->facility_point==0?'checked':''?>>Belum Ada Fasilitas</label>
								<label><input type="radio" value="1" name="fasilitas_pendukung" <?=$wisata->facility_point==1?'checked':''?>>Fasilitas menanggung asuransi</label>
								<label><input type="radio" value="2" name="fasilitas_pendukung" <?=$wisata->facility_point==2?'checked':''?>>Fasilitas menanggung asuransi dan guide</label>
								<label><input type="radio" value="3" name="fasilitas_pendukung" <?=$wisata->facility_point==3?'checked':''?>>Fasilitas menanggung asuransi, guide, dan rest area</label>
							</div>
						</div>
					</div>
					<div class="col-lg-12 col-xs-12">
						<div style="width:50%;float:right">
							<div class="form-group">
								<label >Fasilitas Pendukung 2</label><br/>
								<label><input type="radio" value="3" name="fasilitas_pendukung2" <?=$wisata->facility_point2==3?'checked':''?>>Belum Tersedia Layanan Mobile Provider</label>
								<label><input type="radio" value="2" name="fasilitas_pendukung2" <?=$wisata->facility_point2==2?'checked':''?>>Tersedia Beberapa Layanan Mobile Provider Saja</label>
								<label><input type="radio" value="1" name="fasilitas_pendukung2" <?=$wisata->facility_point2==1?'checked':''?>>Tersedia Semua Layanan Mobile Provider</label><br/>
								<label><input type="radio" value="0" name="fasilitas_pendukung2" <?=$wisata->facility_point2==0?'checked':''?>>Tersedia Layanan 4G</label>
							</div>
						</div>
						<div style="width:50%;float:left">
							<div class="form-group">
								<label >Data Pendukung</label><br/>
								<label><input type="radio" value="3" name="jarak_point" <?=$wisata->jarak_point==3?'checked':''?>>Hanya Bisa Diakses Kendaraan Roda 2</label>
								<label><input type="radio" value="2" name="jarak_point" <?=$wisata->jarak_point==2?'checked':''?>>Bisa Diakses Kendaraan Roda 4</label>
								<label><input type="radio" value="1" name="jarak_point" <?=$wisata->jarak_point==1?'checked':''?>>Bisa Diakses Kendaraan Umum Secara Tidak Langsung</label>
								<label><input type="radio" value="0" name="jarak_point" <?=$wisata->jarak_point==0?'checked':''?>>Langsung Bisa Diakses Kendaraan Umum</label>
							</div>
						</div>						
					</div>
					<div class="col-lg-12 col-xs-12" style="float:left">
						<div style="width:50%;float:left">
							<div class="form-group">
								<label >Type</label><br/>
								<?php 
									foreach($type as $t){ 
										if(in_array($t->id_type,$ta_type)){
								?>
											<label class="checkbox-inline"><input type="checkbox" name="jenis[]" value="<?=$t->id_type?>" checked><?=$t->type_name?></label><br/>
								<?php 
										}else{ ?>
										<label class="checkbox-inline"><input type="checkbox" name="jenis[]" value="<?=$t->id_type?>"><?=$t->type_name?></label><br/>
								<?php
										}
									} 
								?>
							</div>
						</div>
						<div style="width:50%;float:right">
							<div class="form-group">
								<label >Data Pendukung</label><br/>
								<label><input type="radio" value="1" name="tipe_pendukung" <?=$wisata->type_point==1?'checked':''?>>Wisata yang baru dikenal khalayak umum</label>
								<label><input type="radio" value="2" name="tipe_pendukung" <?=$wisata->type_point==2?'checked':''?>>Wisata yang baru dikenal khalayak umum, dan media lokal</label>
								<label><input type="radio" value="3" name="tipe_pendukung" <?=$wisata->type_point==3?'checked':''?>>Wisata yang baru dikenal khalayak umum dan media nasional</label>
								<label><input type="radio" value="4" name="tipe_pendukung" <?=$wisata->type_point==4?'checked':''?>>Wisata yang baru dikenal khalayak umum, media nasional, dan media internasional</label>
							</div>
						</div>						
					</div>
					<!--
					<div class="col-lg-6 col-xs-12">
						<div class="form-group">
							<label for="exampleInputEmail1">Daerah</label>
							<select name="daerah" class="form-control">
								<option value="1">Kota Malang</option>
								<option value="2">Kota Batu</option>
								<option value="3">Kab. Malang</option>
							</select>
						</div>
					</div>
					-->
					<br/>
					<div class="col-lg-12 col-xs-12" style="float:left">
						<table>
							<?php foreach($gambar as $y=>$g){ ?>
								<tr id="gambar-<?=$y+1?>" class="gambar">
									<td>Image</td>
									<td>
										<img src="<?=base_url('asset/images/wisata/').$g->image_src ;?>" alt="" style="height:100px;width:100px"> <br/>
										<input type="hidden" class="form-control" name="id_gambar[]" id="id-gambar-<?=$y+1?>" value="<?=$g->image_id?>" >
										<input type="file" name="slider[]" size="20" multiple="multiple" />
									</td>
									<td>
										<a class="btn btn-plus-image-<?=$y+1?> hide" onclick="tambahKamar_image(<?=$y+1?>)">
											<i class="fa fa-plus fa-lg"></i>
										</a>
										<a class="btn btn-minus-image-<?=$y+1?> hide" onclick="delete_row_image(<?=$y+1?>)" >
											<i class="fa fa-minus fa-lg"></i>
										</a>
									</td>
								</tr>
							<?php } ?>
						</table>
					</div>
					<div class="col-lg-4 col-xs-12" style="float:left">
						<input type="hidden" class="form-control" name="array_hapus" id="hapus" >
						<input type="submit" class="btn btn-primary btn-block" value="Submit">
					</div>
				<?php echo form_close();?>
			</div>        
      </div>
    </div>
	<script type="text/javascript">
		$( document ).ready(function() {
			atur();
			atur_image();
		});
	</script>
	<script>
		function delete_row(id){
			$("#kamar-"+id).remove();
			atur();
		}
	</script>
	<script>
		var arrayHapus = [];
		function delete_row_image(id){
			var hapus = $('#id-gambar-'+id).val();
			arrayHapus.push(hapus);
			$('#hapus').val(arrayHapus);
			$("#gambar-"+id).remove();
			atur_image();
		}
	</script>
	
	<script>
		function tambahKamar(id){
			var data = 	'<tr id="kamar-'+(id+1)+'" class="kamar">'
						+'<td>Nama Kamar</td>'
						+'<td><input type="text" class="form-control" name="kamar[]"  placeholder="Nama"></td>'
						+'<td>Tarif</td>'
						+'<td><input type="text" class="form-control" name="tarif[]" oninput="this.value=addCommas(this.value);" placeholder="Tarif"></td>'
						+'<td>'
						+'<a class="btn btn-plus-'+(id+1)+' hide" onclick="tambahKamar('+(id+1)+')" ><i class="fa fa-plus fa-lg"></i></a>'
						+'<a class="btn btn-minus-'+(id+1)+' hide" onclick="delete_row('+(id+1)+')" ><i class="fa fa-minus fa-lg"></i></a>'
						+'</td>'
						+'</tr>';
			$(data).insertAfter($('#kamar-'+id));
			atur();
		}
	</script>
	<script>
		function tambahKamar_image(id){
			var data = 	'<tr id="gambar-'+(id+1)+'" class="gambar">'
						+'<td>Image</td>'
						+'<td><input type="file" name="slider[]" size="20" multiple="multiple" /></td>'
						+'<td>'
						+'<a class="btn btn-plus-image-'+(id+1)+' hide" onclick="tambahKamar_image('+(id+1)+')"><i class="fa fa-plus fa-lg"></i></a>'
						+'<a class="btn btn-minus-image-'+(id+1)+' hide" onclick="delete_row_image('+(id+1)+')" ><i class="fa fa-minus fa-lg"></i></a>'
						+'</td></tr>';
			$(data).insertAfter($('#gambar-'+id));
			atur_image();
		}
	</script>
	<script>
		function atur(){
			var jumlah = $('.kamar').length;			
			var x=0;
			$('.kamar').each(function( index ) {
				var id = $(this).attr('id');
				var res = id.split("-");
				x++;
				
				if(jumlah == 1){
					//console.log(res[1]);
					$('.btn-plus-'+res[1]).removeClass('hide');
					$('.btn-minus-'+res[1]).addClass('hide');
				}else{
					if(x==jumlah){
						$('.btn-plus-'+res[1]).removeClass('hide');
						$('.btn-minus-'+res[1]).removeClass('hide');
					}else{
						$('.btn-plus-'+res[1]).addClass('hide');
						$('.btn-minus-'+res[1]).removeClass('hide');
					}
				}
			});
		}
	</script>
	<script>
		function atur_image(){
			var jumlah = $('.gambar').length;			
			var x=0;
			$('.gambar').each(function( index ) {
				var id = $(this).attr('id');
				var res = id.split("-");
				x++;
				
				if(jumlah == 1){
					//console.log(res[1]);
					$('.btn-plus-image-'+res[1]).removeClass('hide');
					$('.btn-minus-image-'+res[1]).addClass('hide');
				}else{
					if(x==jumlah){
						$('.btn-plus-image-'+res[1]).removeClass('hide');
						$('.btn-minus-image-'+res[1]).removeClass('hide');
					}else{
						$('.btn-plus-image-'+res[1]).addClass('hide');
						$('.btn-minus-image-'+res[1]).removeClass('hide');
					}
				}
			});
		}
	</script>
	<script>
		function myIP(){
			var geocoder =  new google.maps.Geocoder();
				geocoder.geocode( { 'address': $('#alamat').val()}, function(results, status) {
				if (status == google.maps.GeocoderStatus.OK) {
					$('#langlong').val(results[0].geometry.location.lat() + "," +results[0].geometry.location.lng()); 
				} else {
					$('#langlong').val("Something got wrong " + status);
				}
			});
		}
	
	</script>
	<script type="text/javascript">
	function addCommas(s)
	{
		
		s = s.replace(/,/g, "");
			
			if(isNaN(s)){
				return s;
			}else{
				if(s.length < 3 ){
					return s;
				}else{
					
					var i = s.length % 3;
					var parts = i ? [ s.substr( 0, i ) ] : [];
					for( ; i < s.length ; i += 3 )
					{
						parts.push( s.substr( i, 3 ) );
					}
					var jadi = '';
					for(i=0;i<parts.length;i++){
						if(jadi == ''){
							jadi = jadi+parts[i];
						}else{
							jadi = jadi+','+parts[i];
						}
					}
					//console.log(parts);
					return jadi;
				}
			}
	}
  </script>