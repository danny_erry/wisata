<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Hotel</li>
      </ol>
      <!-- Example DataTables Card-->
    <div class="card mb-3">
        <div class="card-header">
			<div class="col-lg-3 col-xs-12">
				<a href="<?=base_url('wisata/insert')?>"><button class="btn btn-primary btn-block" >Add</button></a>
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
						<thead>
							<tr>
								<th>No</th>
								<th>Nama Tempat Wista</th>
								<th>Deskripsi Tempat Wisata</th>
								<th>Alamat</th>
								<th>Option</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($wisata as $x=>$s){ ?>
								<tr>
									<td><?=$x+1?></td>
									<td><?=$s->ta_name;?></td>
									<td><?=$s->ta_address;?></td>
									<td><?=$s->ta_desc;?></td>
									<td>
										<a href="<?=base_url('wisata/edit/'.$s->ta_id)?>" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
									</td>
								</tr>
							<?php } ?>
					
						</tbody>
					</table>
				</div>
			</div>        
      </div>
    </div>