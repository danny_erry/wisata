<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>SB Admin - Start Bootstrap Template</title>
  <script src="<?=base_url('asset/'); ?>vendor/jquery/jquery.min.js"></script>
  <link rel="stylesheet" href="<?=base_url('asset/dist/css/jquery.datepick.css')?>" type="text/css">
  <!-- Bootstrap core CSS-->
  <link href="<?=base_url('asset/'); ?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="<?=base_url('asset/'); ?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link href="<?=base_url('asset/'); ?>vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="<?=base_url('asset/'); ?>css/sb-admin.css" rel="stylesheet">
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDieAQ8Zk7oTOOskC_i_8TQFH8gBaVBEzo" type="text/javascript"></script>
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
		<a class="navbar-brand" href="index.html">Start Bootstrap</a>
		<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarResponsive">
			<ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
				<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
					<a class="nav-link" href="<?=base_url()?>">
						<i class="fa fa-fw fa-dashboard"></i>
						<span class="nav-link-text">Dashboard</span>
					</a>
				</li>
				<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Components">
					<a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseComponents" data-parent="#exampleAccordion">
						<i class="fa fa-fw fa-wrench"></i>
						<span class="nav-link-text">Master</span>
					</a>
					<ul class="sidenav-second-level collapse" id="collapseComponents">
						<li>
							<a href="<?=base_url('fasilitas')?>">Fasilitas</a>
						</li>
						<li>
							<a href="<?=base_url('type')?>">Type</a>
						</li>
					</ul>
				</li>
				<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Slider">
					<a class="nav-link" href="<?=base_url('slider')?>">
						<i class="fa fa-fw fa-file"></i>
						<span class="nav-link-text">Slider</span>
					</a>
				</li>
				<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Hotel">
					<a class="nav-link" href="<?=base_url('Hotel')?>">
						<i class="fa fa-building-o" aria-hidden="true"></i>
						<span class="nav-link-text">Hotel</span>
					</a>
				</li>
				<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Tempat Wisata">
					<a class="nav-link" href="<?=base_url('wisata')?>">
						<i class="fa fa-suitcase" aria-hidden="true"></i>
						<span class="nav-link-text">Tempat Wisata</span>
					</a>
				</li>
				<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Event">
					<a class="nav-link" href="<?=base_url('event')?>">
						<i class="fa fa-suitcase" aria-hidden="true"></i>
						<span class="nav-link-text">Event</span>
					</a>
				</li>
			</ul>
		</div>
	</nav>