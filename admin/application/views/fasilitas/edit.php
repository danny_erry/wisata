<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Fasilitas</li>
      </ol>
      <!-- Example DataTables Card-->
    <div class="card mb-3">
        <div class="card-header">
			<div class="card-body">
				<form action="<?=base_url('fasilitas/do_update');?>" method="post" >
					<div class="col-lg-3 col-xs-12">
						<div class="form-group">
							<label for="exampleInputEmail1">Nama Fasilitas</label>
							<input type="hidden" name="id" value="<?=$fasilitas->ta_facility?>">
							<input class="form-control" id="exampleInputEmail1" type="text" aria-describedby="emailHelp" name="nama" value="<?=$fasilitas->facility_name?>" placeholder="Nama Fasilitas">
						</div>
					</div>
					
					<div class="col-lg-2 col-xs-12">
						<button type="submit" class="btn btn-primary btn-block" >Update</button>
					</div>
				</form>
			</div>        
      </div>
    </div>