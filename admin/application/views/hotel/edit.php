<style>
	.hide{
		display:none;
	}
</style>
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Hotel</li>
      </ol>
      <!-- Example DataTables Card-->
    <div class="card mb-3">
        <div class="card-header">
			<div class="card-body">
				 <?php echo form_open_multipart('hotel/do_update');?>
					
					<div class="col-lg-6 col-xs-12">
						<div class="form-group">
							<label for="exampleInputEmail1">Nama Hotel</label>
							<input type="text" class="form-control" name="name" value="<?=$hotel->hotel_name?>"  placeholder="Nama">
							<input type="hidden" class="form-control" name="id" value="<?=$hotel->hotel_id?>" >
						</div>
					</div>
					
					<div class="col-lg-6 col-xs-12">
						<div class="form-group">
							<label for="exampleInputEmail1">Deskripsi</label>
							<textarea class="form-control" rows="5" name="desc"><?=$hotel->hotel_desc?></textarea>
						</div>
					</div>
					<div class="col-lg-6 col-xs-12">
						<div class="form-group">
							<label >Alamat</label>
							<input type="text" class="form-control" name="alamat" id="alamat" value="<?=$hotel->hotel_address?>"  placeholder="alamat" onchange="myIP()">
							<input type="hidden" class="form-control" name="langlong" id='langlong' value="<?=$hotel->hotel_lat?>,<?=$hotel->hotel_long?>"  placeholder="alamat">
						</div>
					</div>
					<!--
					<div class="col-lg-6 col-xs-12">
						<div class="form-group">
							<label for="exampleInputEmail1">Daerah</label>
							<select name="daerah" class="form-control">
								<option value="1">Kota Malang</option>
								<option value="2">Kota Batu</option>
								<option value="3">Kab. Malang</option>
							</select>
						</div>
					</div>
					-->
					<div class="col-lg-12 col-xs-12" >
						<table>
							<?php foreach($kamar as $x=>$k){ ?>
								<tr id="kamar-<?=$x+1?>" class="kamar">
									<td>Nama Kamar</td>
									<td><input type="text" class="form-control" name="kamar[]"  placeholder="Nama" value="<?=$k->jenis_kamar_hotel?>" ></td>
									<td>Tarif</td>
									<td><input type="text" class="form-control" name="tarif[]" value="<?=number_format($k->harga)?>" oninput="this.value=addCommas(this.value);" placeholder="Tarif"></td>
									<td>
										<a class="btn btn-plus-<?=$x+1?> hide" onclick="tambahKamar(<?=$x+1?>)">
											<i class="fa fa-plus fa-lg"></i>
										</a>
										<a class="btn btn-minus-<?=$x+1?> hide" onclick="delete_row(<?=$x+1?>)" >
											<i class="fa fa-minus fa-lg"></i>
										</a>
									</td>
								</tr>
							<?php } ?>
						</table>
					</div>
					<br/>
					<div class="col-lg-12 col-xs-12" >
						<table>
							<?php foreach($gambar as $y=>$g){ ?>
								<tr id="gambar-<?=$y+1?>" class="gambar">
									<td>Image</td>
									<td>
										<img src="<?=base_url('asset/images/hotel/').$g->hotel_image_src ;?>" alt="" style="height:100px;width:100px"> <br/>
										<input type="hidden" class="form-control" name="id_gambar[]" id="id-gambar-<?=$y+1?>" value="<?=$g->hotel_image_id?>" >
										<input type="file" name="slider[]" size="20" multiple="multiple" />
									</td>
									<td>
										<a class="btn btn-plus-image-<?=$y+1?> hide" onclick="tambahKamar_image(<?=$y+1?>)">
											<i class="fa fa-plus fa-lg"></i>
										</a>
										<a class="btn btn-minus-image-<?=$y+1?> hide" onclick="delete_row_image(<?=$y+1?>)" >
											<i class="fa fa-minus fa-lg"></i>
										</a>
									</td>
								</tr>
							<?php } ?>
						</table>
					</div>
					<input type="hidden" class="form-control" name="array_hapus" id="hapus" >
					<div class="col-lg-2 col-xs-12">
						<input type="submit" class="btn btn-primary btn-block" value="Submit">
					</div>
				<?php echo form_close();?>
			</div>        
      </div>
    </div>
	<script type="text/javascript">
		$( document ).ready(function() {
			atur();
			atur_image();
		});
	</script>
	<script>
		function delete_row(id){
			$("#kamar-"+id).remove();
			atur();
		}
	</script>
	<script>
		var arrayHapus = [];
		function delete_row_image(id){
			var hapus = $('#id-gambar-'+id).val();
			arrayHapus.push(hapus);
			$('#hapus').val(arrayHapus);
			$("#gambar-"+id).remove();
			atur_image();
		}
	</script>
	
	<script>
		function tambahKamar(id){
			var data = 	'<tr id="kamar-'+(id+1)+'" class="kamar">'
						+'<td>Nama Kamar</td>'
						+'<td><input type="text" class="form-control" name="kamar[]"  placeholder="Nama"></td>'
						+'<td>Tarif</td>'
						+'<td><input type="number" min=1 class="form-control" name="tarif[]" placeholder="Tarif"></td>'
						+'<td>'
						+'<a class="btn btn-plus-'+(id+1)+' hide" onclick="tambahKamar('+(id+1)+')" ><i class="fa fa-plus fa-lg"></i></a>'
						+'<a class="btn btn-minus-'+(id+1)+' hide" onclick="delete_row('+(id+1)+')" ><i class="fa fa-minus fa-lg"></i></a>'
						+'</td>'
						+'</tr>';
			$(data).insertAfter($('#kamar-'+id));
			atur();
		}
	</script>
	<script>
		function tambahKamar_image(id){
			var data = 	'<tr id="gambar-'+(id+1)+'" class="gambar">'
						+'<td>Image</td>'
						+'<td><input type="hidden" class="form-control" id="id-gambar-'+(id+1)+'" name="id_gambar[]"  ><input type="file" name="slider[]" size="20" multiple="multiple" /></td>'
						+'<td>'
						+'<a class="btn btn-plus-image-'+(id+1)+' hide" onclick="tambahKamar_image('+(id+1)+')"><i class="fa fa-plus fa-lg"></i></a>'
						+'<a class="btn btn-minus-image-'+(id+1)+' hide" onclick="delete_row_image('+(id+1)+')" ><i class="fa fa-minus fa-lg"></i></a>'
						+'</td></tr>';
			$(data).insertAfter($('#gambar-'+id));
			atur_image();
		}
	</script>
	<script>
		function atur(){
			var jumlah = $('.kamar').length;			
			var x=0;
			$('.kamar').each(function( index ) {
				var id = $(this).attr('id');
				var res = id.split("-");
				x++;
				
				if(jumlah == 1){
					//console.log(res[1]);
					$('.btn-plus-'+res[1]).removeClass('hide');
					$('.btn-minus-'+res[1]).addClass('hide');
				}else{
					if(x==jumlah){
						$('.btn-plus-'+res[1]).removeClass('hide');
						$('.btn-minus-'+res[1]).removeClass('hide');
					}else{
						$('.btn-plus-'+res[1]).addClass('hide');
						$('.btn-minus-'+res[1]).removeClass('hide');
					}
				}
			});
		}
	</script>
	<script>
		function atur_image(){
			var jumlah = $('.gambar').length;			
			var x=0;
			$('.gambar').each(function( index ) {
				var id = $(this).attr('id');
				var res = id.split("-");
				x++;
				
				if(jumlah == 1){
					//console.log(res[1]);
					$('.btn-plus-image-'+res[1]).removeClass('hide');
					$('.btn-minus-image-'+res[1]).addClass('hide');
				}else{
					if(x==jumlah){
						$('.btn-plus-image-'+res[1]).removeClass('hide');
						$('.btn-minus-image-'+res[1]).removeClass('hide');
					}else{
						$('.btn-plus-image-'+res[1]).addClass('hide');
						$('.btn-minus-image-'+res[1]).removeClass('hide');
					}
				}
			});
		}
	</script>
	<script>
		function myIP(){
			var geocoder =  new google.maps.Geocoder();
				geocoder.geocode( { 'address': $('#alamat').val()}, function(results, status) {
				if (status == google.maps.GeocoderStatus.OK) {
					$('#langlong').val(results[0].geometry.location.lat() + "," +results[0].geometry.location.lng()); 
				} else {
					$('#langlong').val("Something got wrong " + status);
				}
			});
		}
	
	</script>
	<script type="text/javascript">
	function addCommas(s)
	{
		
		s = s.replace(/,/g, "");
			
			if(isNaN(s)){
				return s;
			}else{
				if(s.length < 3 ){
					return s;
				}else{
					
					var i = s.length % 3;
					var parts = i ? [ s.substr( 0, i ) ] : [];
					for( ; i < s.length ; i += 3 )
					{
						parts.push( s.substr( i, 3 ) );
					}
					var jadi = '';
					for(i=0;i<parts.length;i++){
						if(jadi == ''){
							jadi = jadi+parts[i];
						}else{
							jadi = jadi+','+parts[i];
						}
					}
					//console.log(parts);
					return jadi;
				}
			}
	}
  </script>