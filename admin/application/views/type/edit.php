<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Tipe</li>
      </ol>
      <!-- Example DataTables Card-->
    <div class="card mb-3">
        <div class="card-header">
			<div class="card-body">
				<form action="<?=base_url('type/do_update');?>" method="post" >
					<div class="col-lg-3 col-xs-12">
						<div class="form-group">
							<label for="exampleInputEmail1">Tiep Tempat Wisata</label>
							<input type="hidden" name="id" value="<?=$type->id_type?>">
							<input class="form-control" id="exampleInputEmail1" type="text" aria-describedby="emailHelp" name="nama" value="<?=$type->type_name?>" placeholder="Tipe Tempat Wisata">
						</div>
					</div>
					
					<div class="col-lg-2 col-xs-12">
						<button type="submit" class="btn btn-primary btn-block" >Update</button>
					</div>
				</form>
			</div>        
      </div>
    </div>