<style>
	.hide{
		display:none;
	}
</style>
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Event</li>
      </ol>
      <!-- Example DataTables Card-->
    <div class="card mb-3">
        <div class="card-header">
			<div class="row">
				 <?php echo form_open_multipart('event/do_update');?>
					
					<div class="col-lg-12 col-xs-12">
						<div class="form-group">
							<label for="exampleInputEmail1">Nama Event</label>
							<input type="text" class="form-control" name="nama" value="<?=$event->event_name;?>" placeholder="nama" >
							<input type="text" class="form-control" name="id" value="<?=$event->event_id;?>"  >
							<input type="text" class="form-control" name="file_name" value="<?=$event->event_image;?>"  >
						</div>
					</div>
					
					<div class="col-lg-12 col-xs-12">
						<div class="form-group">
							<label for="exampleInputEmail1">Deskripsi</label>
							<textarea class="form-control" rows="5" name="desc"><?=$event->event_desc;?></textarea>
						</div>
					</div>
					<div class="col-lg-12 col-xs-12">
						<div class="form-group">
							<label>Tanggal Mulai</label>
							<input type='text' name="mulai" class="form-control" id='datetimepicker1' value="<?=date('m/d/Y',strtotime($event->event_start));?>" />
						</div>
						<script type="text/javascript">
							$(document).ready(function(){
								$('#datetimepicker1').datepick();
								
							});
						</script>
					</div>
					<div class="col-lg-12 col-xs-12">
						<div class="form-group">
							<label>Tanggal Selesai</label>
							<input type='text' name="selesai" class="form-control" id='datetimepicker2' value="<?=date('m/d/Y',strtotime($event->event_end));?>" />
						</div>
						<script type="text/javascript">
							$(document).ready(function(){
								$('#datetimepicker2').datepick();
								
							});
						</script>
					</div>
					
					<br/>
					<div class="col-lg-12 col-xs-12" style="float:left">
						<table>
							<tr id="gambar-1" class="gambar">
								<td>Image</td>
								<td>
									<img src="<?=base_url('asset/images/event/').$event->event_image ;?>" alt="" style="height:100px;width:100px"> <br/>
									<input type="file" name="slider[]" size="20" multiple="multiple" />
								</td>
							</tr>
						</table>
					</div>
					<div class="col-lg-4 col-xs-12" style="float:left">
						<input type="submit" class="btn btn-primary btn-block" value="Submit">
					</div>
				<?php echo form_close();?>
			</div>        
      </div>
    </div>
	<script type="text/javascript">
		$( document ).ready(function() {
			atur();
			atur_image();
		});
	</script>
	<script>
		function delete_row(id){
			$("#kamar-"+id).remove();
			atur();
		}
	</script>
	<script>
		function delete_row_image(id){
			$("#gambar-"+id).remove();
			atur_image();
		}
	</script>
	
	<script>
		function tambahKamar(id){
			var data = 	'<tr id="kamar-'+(id+1)+'" class="kamar">'
						+'<td>Nama Kamar</td>'
						+'<td><input type="text" class="form-control" name="kamar[]"  placeholder="Nama"></td>'
						+'<td>Tarif</td>'
						+'<td><input type="text" class="form-control" name="tarif[]" oninput="this.value=addCommas(this.value);" placeholder="Tarif"></td>'
						+'<td>'
						+'<a class="btn btn-plus-'+(id+1)+' hide" onclick="tambahKamar('+(id+1)+')" ><i class="fa fa-plus fa-lg"></i></a>'
						+'<a class="btn btn-minus-'+(id+1)+' hide" onclick="delete_row('+(id+1)+')" ><i class="fa fa-minus fa-lg"></i></a>'
						+'</td>'
						+'</tr>';
			$(data).insertAfter($('#kamar-'+id));
			atur();
		}
	</script>
	<script>
		function tambahKamar_image(id){
			var data = 	'<tr id="gambar-'+(id+1)+'" class="gambar">'
						+'<td>Image</td>'
						+'<td><input type="file" name="slider[]" size="20" multiple="multiple" /></td>'
						+'<td>'
						+'<a class="btn btn-plus-image-'+(id+1)+' hide" onclick="tambahKamar_image('+(id+1)+')"><i class="fa fa-plus fa-lg"></i></a>'
						+'<a class="btn btn-minus-image-'+(id+1)+' hide" onclick="delete_row_image('+(id+1)+')" ><i class="fa fa-minus fa-lg"></i></a>'
						+'</td></tr>';
			$(data).insertAfter($('#gambar-'+id));
			atur_image();
		}
	</script>
	<script>
		function atur(){
			var jumlah = $('.kamar').length;			
			var x=0;
			$('.kamar').each(function( index ) {
				var id = $(this).attr('id');
				var res = id.split("-");
				x++;
				
				if(jumlah == 1){
					//console.log(res[1]);
					$('.btn-plus-'+res[1]).removeClass('hide');
					$('.btn-minus-'+res[1]).addClass('hide');
				}else{
					if(x==jumlah){
						$('.btn-plus-'+res[1]).removeClass('hide');
						$('.btn-minus-'+res[1]).removeClass('hide');
					}else{
						$('.btn-plus-'+res[1]).addClass('hide');
						$('.btn-minus-'+res[1]).removeClass('hide');
					}
				}
			});
		}
	</script>
	<script>
		function atur_image(){
			var jumlah = $('.gambar').length;			
			var x=0;
			$('.gambar').each(function( index ) {
				var id = $(this).attr('id');
				var res = id.split("-");
				x++;
				
				if(jumlah == 1){
					//console.log(res[1]);
					$('.btn-plus-image-'+res[1]).removeClass('hide');
					$('.btn-minus-image-'+res[1]).addClass('hide');
				}else{
					if(x==jumlah){
						$('.btn-plus-image-'+res[1]).removeClass('hide');
						$('.btn-minus-image-'+res[1]).removeClass('hide');
					}else{
						$('.btn-plus-image-'+res[1]).addClass('hide');
						$('.btn-minus-image-'+res[1]).removeClass('hide');
					}
				}
			});
		}
	</script>
	<script>
		function myIP(){
			var geocoder =  new google.maps.Geocoder();
				geocoder.geocode( { 'address': $('#alamat').val()}, function(results, status) {
				if (status == google.maps.GeocoderStatus.OK) {
					$('#langlong').val(results[0].geometry.location.lat() + "," +results[0].geometry.location.lng()); 
				} else {
					$('#langlong').val("Something got wrong " + status);
				}
			});
		}
	
	</script>
	<script type="text/javascript">
	function addCommas(s)
	{
		
		s = s.replace(/,/g, "");
			
			if(isNaN(s)){
				return s;
			}else{
				if(s.length < 3 ){
					return s;
				}else{
					
					var i = s.length % 3;
					var parts = i ? [ s.substr( 0, i ) ] : [];
					for( ; i < s.length ; i += 3 )
					{
						parts.push( s.substr( i, 3 ) );
					}
					var jadi = '';
					for(i=0;i<parts.length;i++){
						if(jadi == ''){
							jadi = jadi+parts[i];
						}else{
							jadi = jadi+','+parts[i];
						}
					}
					//console.log(parts);
					return jadi;
				}
			}
	}
  </script>