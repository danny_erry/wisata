<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Slider</li>
      </ol>
      <!-- Example DataTables Card-->
    <div class="card mb-3">
        <div class="card-header">
			<div class="card-body">
				 <?php echo form_open_multipart('slider/do_update');?>
					<div class="col-lg-6 col-xs-12">
						<img src="<?=base_url('asset/images/slider/').$slider->slider_src?>" class="img-responsive" width="250" height="150">
						<input type="file" name="slider" size="20" />
					</div>
					<div class="col-lg-6 col-xs-12">
						<div class="form-group">
							<label for="exampleInputEmail1">Nama Slider</label>
							<input type="text" class="form-control" name="name" value="<?=$slider->slider_caption?>"  placeholder="Nama">
							<input type="hidden" class="form-control" name="id" value="<?=$slider->id_slider?>" >
						</div>
					</div>
					
					<div class="col-lg-6 col-xs-12">
						<div class="form-group">
							<label for="exampleInputEmail1">Deskripsi</label>
							<textarea class="form-control" rows="5" name="desc"><?=$slider->slider_desc?></textarea>
						</div>
					</div>
					
					<div class="col-lg-2 col-xs-12">
						<input type="submit" class="btn btn-primary btn-block" value="Submit">
					</div>
				<?php echo form_close();?>
			</div>        
      </div>
    </div>