<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wisata extends CI_Controller {
	function __construct() {
        parent::__construct();
		$this->load->file('asset/imageupload.php');
        $this->load->helper(array('form', 'url'));
        $this->load->model('wisata_model');
    }
	
	function _view($template = '',$param = ''){
		$data = $this->session->userdata('login');
		if(isset($data)){
			$this->load->view('core/header',$param);
			$this->load->view($template,$param);
			$this->load->view('core/footer');
		}else{
			$this->load->view('login',$template);
		}
	}
	
	function index(){
		$data['wisata'] = $this->wisata_model->get_wisata()->result();
		$this->_view('wisata/index',$data);
	}
	
	function insert(){
		$data['type'] = $this->wisata_model->get_type()->result();
		$data['fasilitas'] = $this->wisata_model->get_fasilitas()->result();
		$this->_view('wisata/insert',$data);
	}
	
	function do_insert(){
		$latlong = explode(',',$this->input->post('langlong'));
		//var_dump($latlong); die();
		$jenis = $this->input->post('jenis');
		$fasilitas = $this->input->post('fasilitas');
		
		$data = array(
			'ta_name' => $this->input->post('nama'),
			'ta_desc' => $this->input->post('desc'),
			'ta_address' => $this->input->post('alamat'),
			'ticket_price' => str_replace(',','',$this->input->post('tarif')),
			'ta_lat' => $latlong[0],
			'ta_long' => $latlong[1],
			'type_point' => $this->input->post('tipe_pendukung'),
			'facility_point' => $this->input->post('fasilitas_pendukung'),
			'jarak_point' => $this->input->post('jarak_point'),
			'facility_point2' => $this->input->post('fasilitas_pendukung2'),
		);
		
		//var_dump($this->input->post('fasilitas')); die();
		$id = $this->wisata_model->do_insert($data);
		for($i=0;$i<count($jenis);$i++){
			$data = array(
				'ta_id' => $id,
				'type_id' => $jenis[$i]
			);
			$this->wisata_model->do_insert_type($data);
		}
		
		for($i=0;$i<count($fasilitas);$i++){
			$data = array(
				'ta_id' => $id,
				'ta_facility' => $fasilitas[$i]
			);
			$this->wisata_model->do_insert_fasilitas($data);
		}
		
		
		$nama = count($_FILES['slider']['name']);
		for($i=0;$i<$nama;$i++){
            $file_name = $_FILES['slider']['name'][$i];
            $files['name'] = $_FILES['slider']['name'][$i];
            $files['type'] = $_FILES['slider']['type'][$i];
            $files['tmp_name'] = $_FILES['slider']['tmp_name'][$i];
            imageupload::uploadImageCreateClone($file_name, $files, 1300, 'asset/images/wisata/');  
			
			$data = array(
				'ta_id' => $id,
				'image_src' => $file_name
			);
			$this->wisata_model->do_insert_gambar($data);
			
		}
		
		redirect('wisata');
	}
	
	function edit($id){
		$data['wisata'] = $this->wisata_model->get_tempat_wisata_by_id($id);
		$type = $this->wisata_model->get_tempat_type_wisata_by_id($id);
		$arrayType = array();
		foreach($type as $t){
			$arrayType[] = $t->type_id;
		}
		$data['ta_type'] = $arrayType;
		
		$fasilitas = $this->wisata_model->get_tempat_fasilitas_wisata_by_id($id);
		$arrayFasilitas = array();
		foreach($fasilitas as $f){
			$arrayFasilitas[] = $f->ta_facility;
		}
		
		$data['ta_fasilitas'] = $arrayFasilitas;
		$data['gambar'] = $this->wisata_model->get_tempat_gambar_wisata_by_id($id);
		$data['type'] = $this->wisata_model->get_type()->result();
		$data['fasilitas'] = $this->wisata_model->get_fasilitas()->result();
		$this->_view('wisata/edit',$data);
	}
	
	function do_update(){
		$latlong = explode(',',$this->input->post('langlong'));
		$id =  $this->input->post('id');
		//var_dump($latlong); die();
		$jenis = $this->input->post('jenis');
		$fasilitas = $this->input->post('fasilitas');
		
		$data = array(
			'ta_name' => $this->input->post('nama'),
			'ta_desc' => $this->input->post('desc'),
			'ta_address' => $this->input->post('alamat'),
			'ticket_price' => str_replace(',','',$this->input->post('tarif')),
			'ta_lat' => $latlong[0],
			'ta_long' => $latlong[1],
			'type_point' => $this->input->post('tipe_pendukung'),
			'facility_point' => $this->input->post('fasilitas_pendukung'),
			'jarak_point' => $this->input->post('jarak_point'),
			'facility_point2' => $this->input->post('fasilitas_pendukung2')
		);
		$this->wisata_model->do_update($id,$data);
		
		$this->wisata_model->hapus_type($id);
		for($i=0;$i<count($jenis);$i++){
			$data = array(
				'ta_id' => $id,
				'type_id' => $jenis[$i]
			);
			$this->wisata_model->do_insert_type($data);
		}
		
		$this->wisata_model->hapus_fasilitas($id);
		for($i=0;$i<count($fasilitas);$i++){
			$data = array(
				'ta_id' => $id,
				'ta_facility' => $fasilitas[$i]
			);
			$this->wisata_model->do_insert_fasilitas($data);
		}
		
		
		$hapus = explode(',',$this->input->post('array_hapus'));
		$this->wisata_model->hapus_gambar($hapus);
		$id_gambar = $this->input->post('id_gambar');
		$nama = count($_FILES['slider']['name']);

		for($i=0;$i<$nama;$i++){
			if(!empty($_FILES['slider']['name'][$i])){
				$file_name = $_FILES['slider']['name'][$i];
				$files['name'] = $_FILES['slider']['name'][$i];
				$files['type'] = $_FILES['slider']['type'][$i];
				$files['tmp_name'] = $_FILES['slider']['tmp_name'][$i];
				imageupload::uploadImageCreateClone($file_name, $files, 1300, 'asset/images/wisata/');  
				
				$data = array(
					'ta_id' => $id,
					'image_src' => $file_name
				);
				//var_dump(array_key_exists($i,$id_gambar) && !empty($id_gambar[$i])); die();
				if(array_key_exists($i,$id_gambar) && !empty($id_gambar[$i])){
					$this->wisata_model->do_update_gambar($id_gambar[$i],$data);
				}else{
					$this->wisata_model->do_insert_gambar($data);
				}
			}
			
		}
		
		redirect('wisata');
	}
	
	
	
	
}
?>