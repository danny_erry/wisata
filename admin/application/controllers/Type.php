<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Type extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model('type_model');
    }
	
	function _view($template = '',$param = ''){
		$data = $this->session->userdata('login');
		if(isset($data)){
			$this->load->view('core/header',$param);
			$this->load->view($template,$param);
			$this->load->view('core/footer');
		}else{
			$this->load->view('login',$template);
		}
	}
	
	public function index()
	{
		$data['type'] = $this->type_model->get_type();
		$this->_view('type/index',$data);
	}
	
	public function insert(){
		$this->_view('type/insert');
	}
	
	public function do_insert(){
		$data = array(
			'type_name' => $this->input->post('nama')
		);
		$this->type_model->do_insert($data);
		redirect('type');
	}
	
	public function edit($id){
		$data['type'] = $this->type_model->get_type_by_id($id)->row();
		$this->_view('type/edit',$data);
	}
	
	public function do_update(){
		$data = array(
			'type_name' => $this->input->post('nama')
		);
		$this->type_model->do_update($data,$this->input->post('id'));
		redirect('type');
	}
}	