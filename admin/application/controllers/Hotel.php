<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hotel extends CI_Controller {
	function __construct() {
        parent::__construct();
		$this->load->file('asset/imageupload.php');
        $this->load->helper(array('form', 'url'));
        $this->load->model('hotel_model');
    }
	
	function _view($template = '',$param = ''){
		$data = $this->session->userdata('login');
		if(isset($data)){
			$this->load->view('core/header',$param);
			$this->load->view($template,$param);
			$this->load->view('core/footer');
		}else{
			$this->load->view('login',$template);
		}
	}
	
	function index(){
		$data['hotel'] = $this->hotel_model->get_hotel()->result();
		$this->_view('hotel/index',$data);
	}
	
	function insert(){
		$this->_view('hotel/insert');
	}
	
	private function set_upload_options()
{   
    //upload an image options
		$config = array();
		$config['upload_path'] 	 = './asset/images/hotel/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size']      = '0';
		$config['overwrite']     = FALSE;

		return $config;
	}
	
	function do_insert(){
		$latlong = explode(',',$this->input->post('langlong'));
		
		$data = array(
			'hotel_name' => $this->input->post('name'),
			'hotel_desc' => $this->input->post('desc'),
			'hotel_address' => $this->input->post('alamat'),
			'hotel_lat' => $latlong[0],
			'hotel_long' => $latlong[1],
		);
		$id = $this->hotel_model->do_insert($data);
		$nama = count($_FILES['slider']['name']);
		for($i=0;$i<$nama;$i++){
            $file_name = $_FILES['slider']['name'][$i];
            $files['name'] = $_FILES['slider']['name'][$i];
            $files['type'] = $_FILES['slider']['type'][$i];
            $files['tmp_name'] = $_FILES['slider']['tmp_name'][$i];
            imageupload::uploadImageCreateClone($file_name, $files, 1300, 'asset/images/hotel/');  
			
			$data = array(
				'hotel_id' => $id,
				'hotel_image_src' => $file_name
			);
			$this->hotel_model->do_insert_gambar($data);
			
		}
		
		$kamar =  $this->input->post('kamar');
		$tarif =  $this->input->post('tarif');
		for($i=0;$i<count($kamar);$i++){
			$data =array(
				'hotel_id' => $id,
				'jenis_kamar_hotel' => $kamar[$i],
				'harga' => str_replace(',','',$tarif[$i]),
			);
			$this->hotel_model->do_insert_detail($data);
		}
		redirect('hotel');
	}
	
	function edit($id){
		$data['hotel'] =  $this->hotel_model->get_hotel_by_id($id);
		$data['kamar'] = $this->hotel_model->get_kamar_by_id($id);
		$data['gambar'] = $this->hotel_model->get_gambar_by_id($id);
		$this->_view('hotel/edit',$data);
	}
	
	function do_update(){
		$latlong = explode(',',$this->input->post('langlong'));
		$id = $this->input->post('id');
		$data = array(
			'hotel_name' => $this->input->post('name'),
			'hotel_desc' => $this->input->post('desc'),
			'hotel_address' => $this->input->post('alamat'),
			'hotel_lat' => $latlong[0],
			'hotel_long' => $latlong[1],
		);
		$this->hotel_model->do_update($id,$data);
		$hapus = explode(',',$this->input->post('array_hapus'));
		$this->hotel_model->hapus_gambar($hapus);
		$id_gambar = $this->input->post('id_gambar');
		$nama = count($_FILES['slider']['name']);
		for($i=0;$i<$nama;$i++){
			if(!empty($_FILES['slider']['name'][$i])){
				$file_name = $_FILES['slider']['name'][$i];
				$files['name'] = $_FILES['slider']['name'][$i];
				$files['type'] = $_FILES['slider']['type'][$i];
				$files['tmp_name'] = $_FILES['slider']['tmp_name'][$i];
				imageupload::uploadImageCreateClone($file_name, $files, 1300, 'asset/images/hotel/');  
				
				$data = array(
					'hotel_id' => $id,
					'hotel_image_src' => $file_name
				);
				//var_dump(array_key_exists($i,$id_gambar) && !empty($id_gambar[$i])); die();
				if(array_key_exists($i,$id_gambar) && !empty($id_gambar[$i])){
					$this->hotel_model->do_update_gambar($id_gambar[$i],$data);
				}else{
					$this->hotel_model->do_insert_gambar($data);
				}
			}
			
		}
		$this->hotel_model->delete_kamar($id);
		$kamar =  $this->input->post('kamar');
		$tarif =  $this->input->post('tarif');
		for($i=0;$i<count($kamar);$i++){
			$data =array(
				'hotel_id' => $id,
				'jenis_kamar_hotel' => $kamar[$i],
				'harga' => str_replace(',','',$tarif[$i]),
			);
			$this->hotel_model->do_insert_detail($data);
		}
		redirect('hotel');
	}
	
	
}
?>