<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slider extends CI_Controller {
	function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model('slider_model');
    }
	
	function _view($template = '',$param = ''){
		$data = $this->session->userdata('login');
		if(isset($data)){
			$this->load->view('core/header',$param);
			$this->load->view($template,$param);
			$this->load->view('core/footer');
		}else{
			$this->load->view('login',$template);
		}
	}
	
	function index(){
		$data['slider'] = $this->slider_model->get_slider()->result();
		$this->_view('slider/index',$data);
	}
	
	function insert(){
		$this->_view('slider/insert');
	}
	
	function do_insert(){
		
		$nama = $_FILES['slider']['name'];
		$desc = $this->input->post('desc');
		$name = $this->input->post('name');
		
		$config['upload_path']          = './asset/images/slider/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
		$this->load->library('upload', $config);
		if ( ! $this->upload->do_upload('slider'))
		{
			$error = array('error' => $this->upload->display_errors());
			var_dump($error); die();
		}
		else
		{
			$data = array(
				'slider_desc' => $desc,
				'slider_caption' => $name,
				'slider_src' => $nama
			);
			$this->slider_model->do_insert($data);
			redirect('slider');
		} 
	}
	
	function edit($id){
		$data['slider'] = $this->slider_model->get_slider_by_id($id)->row();
		$this->_view('slider/edit',$data);
	}
	
	function do_update(){
		$nama = $_FILES['slider']['name'];
		$desc = $this->input->post('desc');
		$name = $this->input->post('name');
		$id = $this->input->post('id');
		if(!empty($nama)){
			$config['upload_path']          = './asset/images/slider/';
			$config['allowed_types']        = 'gif|jpg|png';
			$this->load->library('upload', $config);
			if ( ! $this->upload->do_upload('slider'))
			{
				$error = array('error' => $this->upload->display_errors());
				var_dump($error); die();
			}
			else
			{
				$data = array(
					'slider_desc' => $desc,
					'slider_caption' => $name,
					'slider_src' => $nama
				);
				$this->slider_model->do_update($data,$id);
				redirect('slider');
			}
		}else{
			$data = array(
				'slider_desc' => $desc,
				'slider_caption' => $name
			);
			
			$this->slider_model->do_update($data,$id);
			redirect('slider');
		}
		
	}
	
}
?>