<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Event extends CI_Controller {
	function __construct() {
        parent::__construct();
		$this->load->file('asset/imageupload.php');
        $this->load->helper(array('form', 'url'));
        $this->load->model('event_model');
    }
	
	function _view($template = '',$param = ''){
		$data = $this->session->userdata('login');
		if(isset($data)){
			$this->load->view('core/header',$param);
			$this->load->view($template,$param);
			$this->load->view('core/footer');
		}else{
			$this->load->view('login',$template);
		}
	}
	
	function index(){
		$data['event'] = $this->event_model->get_event()->result();
		$this->_view('event/index',$data);
	}
	
	function insert(){
		
		$this->_view('event/insert');
	}
	
	function do_insert(){
		$start = explode('/',$this->input->post('mulai'));
		$end = explode('/',$this->input->post('selesai'));
		
		$nama = count($_FILES['slider']['name']);
		for($i=0;$i<$nama;$i++){
            $file_name = $_FILES['slider']['name'][$i];
            $files['name'] = $_FILES['slider']['name'][$i];
            $files['type'] = $_FILES['slider']['type'][$i];
            $files['tmp_name'] = $_FILES['slider']['tmp_name'][$i];
            imageupload::uploadImageCreateClone($file_name, $files, 1300, 'asset/images/event/'); 			
		}
		
		
		$data = array(
			'event_name' => $this->input->post('nama'),
			'event_desc' => $this->input->post('desc'),
			'event_image' => $file_name,
			'event_start' => $start[2].'-'.$start[0].'-'.$start[1],
			'event_end' => $end[2].'-'.$end[0].'-'.$end[1],
		);
		
		//var_dump($data); die();
		$this->event_model->do_insert($data);
		
		
		
		redirect('event');
	}
	
	function edit($id){
		$data['event'] = $this->event_model->get_tempat_event_by_id($id);
		$this->_view('event/edit',$data);
	}
	
	function do_update(){
		$start = explode('/',$this->input->post('mulai'));
		$end = explode('/',$this->input->post('selesai'));
		$id = $this->input->post('id');
		
		$nama = count($_FILES['slider']['name']);
		
		for($i=0;$i<$nama;$i++){
            $file_name = $_FILES['slider']['name'][$i];
			if(!empty($file_name)){
				$files['name'] = $_FILES['slider']['name'][$i];
				$files['type'] = $_FILES['slider']['type'][$i];
				$files['tmp_name'] = $_FILES['slider']['tmp_name'][$i];
				imageupload::uploadImageCreateClone($file_name, $files, 1300, 'asset/images/event/'); 			
			}else{
				$file_name = $this->input->post('file_name');
			}
		}
		//var_dump($file_name,$id); die();
		
		$data = array(
			'event_name' => $this->input->post('nama'),
			'event_desc' => $this->input->post('desc'),
			'event_image' => $file_name,
			'event_start' => $start[2].'-'.$start[0].'-'.$start[1],
			'event_end' => $end[2].'-'.$end[0].'-'.$end[1],
		);
		$this->event_model->do_update($id,$data);
		
		
		redirect('event');
	}
	
	
	
	
}
?>