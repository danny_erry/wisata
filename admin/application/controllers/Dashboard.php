<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model('dashboard_model');
    } 
	
	function _view($template = '',$param = ''){
		$data = $this->session->userdata('login');
		if(isset($data)){
			$this->load->view('core/header',$param);
			$this->load->view($template,$param);
			$this->load->view('core/footer');
		}else{
			$this->load->view('login',$template);
		}
	}
	
	public function do_login(){
		$username = $this->input->post('user');
		$password = $this->input->post('password');
		$remember = $this->input->post('remember');
		$user = $this->dashboard_model->validate_user($username, md5($password))->row();
		if (!empty($user)) {
			$userData = array(
				'id_user' => $user->id_user,
				'user_name' => $user->user_name
			);

			$this->session->set_userdata('login', $userData);
			redirect('dashboard/index/');
		} else {
			$this->session->set_flashdata('unsuccess', 'Maaf, Nama Akun atau Kata Sandi salah');
			$data = 'error';
			redirect('dashboard');
		}
	}
	
	function logout(){
		$this->session->sess_destroy();
		redirect('dashboard');
	}
	
	public function index()
	{
		$this->_view('home');
	}
}