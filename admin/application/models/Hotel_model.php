<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Hotel_model extends CI_Model {
	
	function get_hotel(){
		return $this->db->get('hotel');
	}
	
	function do_insert($data){
		$this->db->insert('hotel', $data);
		$insert_id = $this->db->insert_id();        
        return  $insert_id;
	}
	
	function do_insert_detail($data){
		$this->db->insert('kamar_hotel', $data);
	}
	
	function do_insert_gambar($data){
		$this->db->insert('hotel_image', $data);
	}
	
	function get_hotel_by_id($id){
		$this->db->select('*');
		$this->db->from('hotel');
		$this->db->where('hotel_id',$id);
		return $this->db->get()->row();
	}
	
	function get_kamar_by_id($id){
		$this->db->select('*');
		$this->db->from('kamar_hotel');
		$this->db->where('hotel_id',$id);
		return $this->db->get()->result();
	}
	
	function get_gambar_by_id($id){
		$this->db->select('*');
		$this->db->from('hotel_image');
		$this->db->where('hotel_id',$id);
		return $this->db->get()->result();
	}
	
	function do_update($id,$data){
		$this->db->where('hotel_id', $id);
		$this->db->update('hotel', $data);
	}
	
	function do_update_gambar($id,$data){
		$this->db->where('hotel_image_id', $id);
		$this->db->update('hotel_image', $data);
	}
	
	function hapus_gambar($data){
		$this->db->where_in('hotel_image_id', $data);
		$this->db->delete('hotel_image');
	}
	
	function delete_kamar($id){
		$this->db->delete('kamar_hotel', array('hotel_id' => $id)); 
	}
	
	
}
?>