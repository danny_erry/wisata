<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Type_model extends CI_Model {

	function get_type(){
		$this->db->select('*');
		$this->db->from('type_tempat_wisata');
		return $this->db->get()->result();
	}
	
	function do_insert($data){
		$this->db->insert('type_tempat_wisata', $data);
	}
	
	function do_update($data,$id){		
		$this->db->where('id_type', $id);
		$this->db->update('type_tempat_wisata', $data);
	}
	
	function get_type_by_id($id){
		return $this->db->get_where('type_tempat_wisata', array('id_type' => $id));
	}


}