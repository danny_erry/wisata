<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Event_model extends CI_Model {
	
	function get_event(){
		return $this->db->get('event');
	}
	
	function get_type(){
		return $this->db->get('type_tempat_event');
	}
	
	function get_fasilitas(){
		return $this->db->get('facility_tempat_event');
	}
	
	function do_insert($data){
		$this->db->insert('event', $data);
		$insert_id = $this->db->insert_id();        
        return  $insert_id;
	}
	
	function do_update($id,$data){
		$this->db->where('event_id', $id);
		$this->db->update('event', $data);
		//var_dump($this->db->last_query()); die();
	}
	
	function do_insert_gambar($data){
		$this->db->insert('ta_image', $data);
	}
	
	function do_update_gambar($id,$data){
		$this->db->where('image_id', $id);
		$this->db->update('ta_image', $data);
	}
	
	function do_insert_type($data){
		$this->db->insert('ta_type', $data);
	}
	
	function do_insert_fasilitas($data){
		$this->db->insert('ta_facility', $data);
	}
	
	function get_tempat_event_by_id($id){
		return $this->db->get_where('event', array('event_id' => $id))->row();
	}
	
	function get_tempat_type_event_by_id($id){
		return $this->db->get_where('ta_type', array('ta_id' => $id))->result();
	}
	
	function get_tempat_fasilitas_event_by_id($id){
		return $this->db->get_where('ta_facility', array('ta_id' => $id))->result();
	}
	
	function get_tempat_gambar_event_by_id($id){
		return $this->db->get_where('ta_image', array('ta_id' => $id))->result();
	}
	
	function hapus_gambar($data){
		$this->db->where_in('image_id', $data);
		$this->db->delete('ta_image');
		//var_dump($this->db->last_query());
	}
	
	function hapus_type($id){
		$this->db->delete('ta_type', array('ta_id' => $id)); 
	}
	
	function hapus_fasilitas($id){
		$this->db->delete('ta_facility', array('ta_id' => $id)); 
	}
	
}
?>