<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Slider_model extends CI_Model {
	
	function get_slider(){
		return $this->db->get('slider');
	}
	
	function do_insert($data){
		$this->db->insert('slider', $data);
	}
	
	function get_slider_by_id($id){
		return $this->db->get_where('slider', array('id_slider' => $id));
	}
	
	function do_update($data,$id){		
		$this->db->where('id_slider', $id);
		$this->db->update('slider', $data);
	}
}
?>