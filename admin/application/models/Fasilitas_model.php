<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fasilitas_model extends CI_Model {

	function get_fasilitas(){
		$this->db->select('*');
		$this->db->from('facility_tempat_wisata');
		return $this->db->get()->result();
	}
	
	function do_insert($data){
		$this->db->insert('facility_tempat_wisata', $data);
	}
	
	function do_update($data,$id){		
		$this->db->where('ta_facility', $id);
		$this->db->update('facility_tempat_wisata', $data);
	}
	
	function get_fasilitas_by_id($id){
		return $this->db->get_where('facility_tempat_wisata', array('ta_facility' => $id));
	}


}