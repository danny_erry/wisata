-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 07, 2018 at 06:15 AM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wisata`
--

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `event_id` int(11) NOT NULL,
  `event_name` varchar(50) NOT NULL,
  `event_desc` text NOT NULL,
  `event_start` date NOT NULL,
  `event_end` date NOT NULL,
  `event_image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`event_id`, `event_name`, `event_desc`, `event_start`, `event_end`, `event_image`) VALUES
(1, 'Event 1', 'aaaaaaaaaaaaaaaaa', '2017-12-03', '2017-12-11', 'DAfqAqgWsAEOwNG.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `facility_tempat_wisata`
--

CREATE TABLE `facility_tempat_wisata` (
  `ta_facility` int(11) NOT NULL,
  `facility_name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `facility_tempat_wisata`
--

INSERT INTO `facility_tempat_wisata` (`ta_facility`, `facility_name`) VALUES
(1, 'Tempat Bermain Anak'),
(2, 'Penginapan'),
(3, 'Restauran'),
(4, 'Tempat Belanja');

-- --------------------------------------------------------

--
-- Table structure for table `hotel`
--

CREATE TABLE `hotel` (
  `hotel_id` int(11) NOT NULL,
  `hotel_name` varchar(30) NOT NULL,
  `hotel_desc` text NOT NULL,
  `hotel_address` text NOT NULL,
  `hotel_lat` varchar(15) NOT NULL,
  `hotel_long` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hotel`
--

INSERT INTO `hotel` (`hotel_id`, `hotel_name`, `hotel_desc`, `hotel_address`, `hotel_lat`, `hotel_long`) VALUES
(1, 'SANTIKA', 'PT Grahawita Santika didirikan sebagai pemilik dan pengelola Santika Indonesia Hotels & Resorts. Hotel pertama yang beroperasi adalah Hotel Santika Bandung dan setelah itu mulai mengembangkan sayap bisnis perhotelan ke kota-kota strategis lainnya di Indonesia, yaitu Jakarta, Bandung, Semarang, Cirebon, Surabaya, Jogjakarta, Pontianak, Kuta dan Seminyak Bali, Manado , Makassar dan lain-lain.', 'Jalan Letjen Sutoyo Nomor 79 Malang', '-7.9582989', '112.63695729999'),
(2, 'TUGU PARK', 'Malang, a cool-weathered town once dubbed ‘the Paris of Southeast Asia’, 800 meters above the sea level is surrounded by lush rolling green hills of apple, tea, coffee plantations, waterfalls and tropical forests. It was once the town where all the Dutch plantation masters retired in beautiful colonial houses. It is a perfect base for visiting the majestic volcano Mount Bromo, and is surrounded by Hindu temples ruins from centuries-old kingdoms', 'Jalan Tugu Nomor 3 Malang', '-7.977551899999', '112.63451809999'),
(3, 'GAJAHMADA GRAHA', 'Three-star hotel located in the middle of Malang city with ETHNIC ROMAWI STYLE 25 minutes from the airport Abdul Rachman Saleh and 10 minutes to ALUN – ALUN MALANG.\r\n\r\nEquipped with 43 Superior rooms and 1 President suite room, and there are two Meeting rooms & 1 big Ballroom very magnificently decorated crystal lamps and Roman sculptures specifically to provide luxury wedding party', 'Jalan Dr. Cipto 17 Malang', '-7.968521999999', '112.63590299999'),
(4, 'GRAHA CAKRA', 'Hotel Graha Cakra Malang merupakan pilihan yang tepat. Selain terletak di lokasi yang strategis, hotel ini juga menyediakan berbagai fasilitas yang lengkap untuk mendukung kenyamanan para tamu saat berada di hotel ini', 'Jalan Cerme 16 Malang', '-7.970300999999', '112.625992'),
(5, 'GRAND PALACE', 'Hotel Grand Palace Malang merupakan tempat yang paling cocok untuk beristirahat di daerah Malang. Hotel ini akan memberikan pelayanan yang terbaik bagi Anda yang berisirahat disini sekaligus dapat menelusuri keindahan kota Malang yang tak pernah ada habisnya. Dengan desain gedung yang indah dan megah dan indah sudah tampak bahwa hotel ini merupakan hotel berkelas dan pasti terjamin mutu pelayanannya. Suasana khas kota Malang pun terasa saat anda beristirahat di hotel ini', 'Jalan Ade Irma Suryani 23 Malang', '-7.9846222', '112.62844710000'),
(6, 'KARTIKA GRAHA', 'Apakah Anda saat ini sedang bingung memilih hotel yang nyaman di pusat kota Malang? Anda tidak perlu bingung karena Hotel Kartika Graha Malang dapat memberi solusi yang mudah dan nyaman sesuai dengan kebutuhan Anda. Hotel Kartika Graha merupakan hotel bintang 4 yang terletak di tengah kota Malang. Hotel ini menawarkan berbagai fasilitas untuk memuaskan setiap tamu yang menginap di hotel ini.', 'Jalan Jakgung Suprapto 17 ', '-7.112103299999', '112.41537629999'),
(7, 'Sahid Montana Malang', 'Hotel Sahid Montana Malang merupakan tempat yang tepat untuk relaksasi dan bersenang – senang setelah menghabiskan hari yang panjang dan melelahkan saat berlibur di Malang. Hotel ini menyediakan pilihan fasilitas yang lengkap dan pelayanan yang ramah untuk membuat liburan Anda di Malang semakin berkesan dan menyenangkan', 'Jalan Kahuripan 9 Malang', '-7.9771984', '112.63291890000'),
(8, 'RICHE', 'Hotel Riche Malang memang bukan hotel baru karena bangunan hotel ini telah berdiri sejak tahun 1934. Namun, setiap usaha telah dilakukan oleh hotel ini agar para tamu yang menginap merasa betah dan nyaman. Untuk mencapai tujuan ini, Hotel Riche telah menyediakan berbagai fasilitas. Di Hotel Riche, setiap usaha dilakukan untuk membuat tamu merasa nyaman. Dan untuk hal ini, Hotel Riche menyediakan pilihan yang terbaik dari sisi pelayanan dan juga perlengkapannya. Beberapa fasilitas top di hotel ini antara lain adalah tempat parkir mobil yang cukup luas, restoran yang menyediakan berbagai pilihan makana lezat, layanan kamar oleh staff profesional, fasilitas ruang rapat , parkir valet, dan beberapa fasilitas menarik lainnya. Hotel ini juga menyediakan beberapa fasilitas rekreasi, salah satunya adalah taman.', 'Jalan Basuki Rachmad 1', '-7.981362000000', '112.63003400000'),
(9, 'SPLENDID INN', 'Hotel paling melegenda di kota Malang', 'Jalan Majapahit 2 - 4 Malang', '-6.3495589', '106.56342949999');

-- --------------------------------------------------------

--
-- Table structure for table `hotel_image`
--

CREATE TABLE `hotel_image` (
  `hotel_image_id` int(11) NOT NULL,
  `hotel_id` int(11) NOT NULL,
  `hotel_image_src` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hotel_image`
--

INSERT INTO `hotel_image` (`hotel_image_id`, `hotel_id`, `hotel_image_src`) VALUES
(1, 1, '240423_17012113510050459608.jpg'),
(2, 1, 'Tarif-Hotel-Santika-Premiere-Malang.jpg'),
(3, 2, '43194164.jpg'),
(4, 2, 'executive-room.jpg'),
(5, 3, '248378_14061313150019837843.jpg'),
(6, 3, 'gajahmada-graha-hotel.jpg'),
(7, 4, '14527153.jpg'),
(8, 4, 'Hotel-Graha-Cakra-Malang.jpg'),
(9, 5, 'Executive-Suite-gp.jpg'),
(10, 5, 'Kamar-Executive-Suite-gp.jpg'),
(11, 6, 'Hotel-Kartika-Graha-Malang1.jpg'),
(12, 7, 'Hotel-Sahid-Montana-Malang.jpg'),
(13, 8, 'Hotel-Riche-Malang.jpg'),
(14, 9, 'Hotel-Splendid-Inn-Malang.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `kamar_hotel`
--

CREATE TABLE `kamar_hotel` (
  `id_kamar_hotel` int(11) NOT NULL,
  `hotel_id` int(11) NOT NULL,
  `jenis_kamar_hotel` varchar(20) NOT NULL,
  `harga` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kamar_hotel`
--

INSERT INTO `kamar_hotel` (`id_kamar_hotel`, `hotel_id`, `jenis_kamar_hotel`, `harga`) VALUES
(1, 1, 'SMART DEAL', 274000),
(2, 1, 'HOT DEAL', 387000),
(3, 1, 'STAY 5 PAY 4', 480000),
(4, 2, 'Superior Delux', 1100000),
(5, 2, 'Aspara residence', 15400000),
(6, 2, 'Raden Saleh Suit', 2550000),
(7, 2, 'Executive Suites', 1725000),
(8, 3, 'Superior Room Only', 411000),
(9, 3, 'Superior', 470000),
(10, 3, 'Presiden Suites', 941000),
(11, 4, 'Superior', 430000),
(12, 4, 'Duluxe', 625000),
(13, 4, 'Junior Suite', 855000),
(14, 4, 'Royal Suite', 1485000),
(15, 5, 'Superior', 625000),
(16, 5, 'Deluxe', 685000),
(17, 5, 'Executive Suite', 745000),
(18, 5, 'Presiden Suites', 1975000),
(19, 6, 'Superior', 423000),
(20, 6, 'Deluxe', 459000),
(21, 6, 'Eksekuti', 621000),
(22, 6, 'Eksekutif Suite', 722000),
(23, 7, 'deluxe', 367000),
(24, 8, 'Deluxe', 247000),
(25, 9, 'Deluxe', 231000);

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `id_slider` int(11) NOT NULL,
  `slider_src` text NOT NULL,
  `slider_caption` varchar(50) NOT NULL,
  `slider_desc` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id_slider`, `slider_src`, `slider_caption`, `slider_desc`) VALUES
(1, 'calm-beach-waves-wallpaper-301-345-hd-wallpapers.jpg', 'Calm Beach 2', 'Calm Beach Desc Calm Beach Desc Calm Beach Desc Calm Beach Desc Calm Beach Desc Calm Beach Desc Calm Beach Desc Calm Beach Desc Calm Beach Desc Calm Beach Desc Calm Beach Desc Calm Beach Desc Calm Beach Desc Calm Beach Desc Calm Beach Desc Calm Beach Desc Calm Beach Desc Calm Beach Desc Calm Beach Desc '),
(2, 'download.jpg', 'Slider 2 ', 'Slider 2 Slider 2 Slider 2 Slider 2 Slider 2 Slider 2 Slider 2 Slider 2 Slider 2 Slider 2 Slider 2 Slider 2 Slider 2 Slider 2 Slider 2 Slider 2 Slider 2 Slider 2 Slider 2 Slider 2 Slider 2 Slider 2 Slider 2 Slider 2 Slider 2 Slider 2 Slider 2 ');

-- --------------------------------------------------------

--
-- Table structure for table `ta_facility`
--

CREATE TABLE `ta_facility` (
  `facility_id` int(11) NOT NULL,
  `ta_id` int(11) NOT NULL,
  `ta_facility` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ta_facility`
--

INSERT INTO `ta_facility` (`facility_id`, `ta_id`, `ta_facility`) VALUES
(1, 1, 1),
(2, 1, 4),
(3, 2, 1),
(4, 3, 1),
(5, 3, 3);

-- --------------------------------------------------------

--
-- Table structure for table `ta_image`
--

CREATE TABLE `ta_image` (
  `image_id` int(11) NOT NULL,
  `ta_id` int(11) NOT NULL,
  `image_src` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ta_image`
--

INSERT INTO `ta_image` (`image_id`, `ta_id`, `image_src`) VALUES
(1, 1, 'Alun_81.jpg'),
(2, 2, 'JATIMTIMES-Suasana-Taman-Trunojoyo-Kota-MalangFRaYj.jpg'),
(3, 3, 'Hawai-Water-Park-Malang-MG.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `ta_type`
--

CREATE TABLE `ta_type` (
  `ta_type_id` int(11) NOT NULL,
  `ta_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ta_type`
--

INSERT INTO `ta_type` (`ta_type_id`, `ta_id`, `type_id`) VALUES
(1, 1, 1),
(2, 1, 5),
(3, 2, 1),
(4, 2, 3),
(5, 2, 5),
(6, 3, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tourist_atraction`
--

CREATE TABLE `tourist_atraction` (
  `ta_id` int(11) NOT NULL,
  `ta_name` varchar(40) NOT NULL,
  `ta_address` text NOT NULL,
  `ticket_price` int(11) NOT NULL,
  `ta_desc` text NOT NULL,
  `ta_lat` varchar(15) NOT NULL,
  `ta_long` varchar(15) NOT NULL,
  `type_point` int(11) NOT NULL,
  `facility_point` int(11) NOT NULL,
  `facility_point2` int(11) NOT NULL,
  `jarak_point` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tourist_atraction`
--

INSERT INTO `tourist_atraction` (`ta_id`, `ta_name`, `ta_address`, `ticket_price`, `ta_desc`, `ta_lat`, `ta_long`, `type_point`, `facility_point`, `facility_point2`, `jarak_point`) VALUES
(1, 'Alun Alun KotaMalang', 'Jalan Merdeka Selatan, Kauman, Klojen, Kiduldalem, Klojen, Kota Malang', 0, 'Alun Alun Kota Malang - Taman Kota Untuk Semuanya - Kota Malang memiliki dua Alun-Alun yang terletak di pusat kota diantaranya alun-alun yang terletak di jalan Merdeka atau depan Masjid Agung Jami’ yang biasanya disebut dengan Alun Alun Kota Malang atau Alun Alun Merdeka atau Alun-Alun Jami’ serta alun-alun yang terletak di depan Balaikota yang biasanya disebut dengan Alun-Alun Tugu atau Alun-Alun Bundar. Dalam tulisan nnoart kali ini hanya akan mengulas mengenai Alun-Alun Malang atau Alun-Alun Merdeka, sedangkan untuk Alun-Alun Tugu, sudah pernah diulas sebelumnya dalam blog ini.\r\n\r\nAlun-Alun Kota Malang dibangun pada tahun 1882  dan merupakan menjadi tempat berkumpulnya warga serta tempat rekreasi yang seru bagi keluarga, terutama setelah mengalami perubahan besar-besaran pada tahun 2015 lalu. Alun-Alun ini tidak pernah sepi pengunjung, hal tersebut karena lokasinya yang sangat strategis di pusat kota dimana terdapat pusat perbelanjaan terkenal Kota Malang seperti Ramayana, Pasar Besar, Mitra, Malang Plaza, Gajah Mada Plaza, Sarinah dsb dan juga terletak tepat di depan Masjid terbesar di Kota Malang, Masjid Agung Jami’ maupun fasilitas perkantoran.', '-7.9834272', '112.63071360000', 4, 1, 0, 0),
(2, 'Tamana Trunojoyo', 'jalan trunojoyo malang', 0, 'Untuk Taman Trunojoyo sendiri adalah taman yang terletak di jalan Trunojoyo Kota Malang, tetapi lebih dikenal oleh masyarakat kota Malang sebagai taman depan Stasiun Kota Baru Malang. Taman ini sebenarnya sudah ada sejak lama,    seingat saya sejak saya kecil taman ini sudah ada, tetapi baru-baru ini saja taman Trunojoya diperbaiki dan dibuat lebih indah dengan banyak fasilitas pendukungnya, sehingga dapat dijadikan sebagai tempat wisata murah meriah bagi warga Malang Raya, maupun juga para wisatawan dari daerah lain', '-7.9771115', '112.63681550000', 2, 1, 0, 0),
(3, 'Hawai Waterpark', 'Jalan Graha Kencana Raya, Banjararum, Singosari', 75000, 'Di Hawai Waterpark anda bisa menikmati dunia air dengan keluarga anda untuk mengisi hari libur dengan menikmati gelombang tsunami buatan. Berikut akan kami sampaikan Harga Tiket Masuk Hawai Waterpark dan juga harga sewa fasilitas yang ada.', '-7.9239518', '112.656878', 4, 2, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `type_tempat_wisata`
--

CREATE TABLE `type_tempat_wisata` (
  `id_type` int(11) NOT NULL,
  `type_name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `type_tempat_wisata`
--

INSERT INTO `type_tempat_wisata` (`id_type`, `type_name`) VALUES
(1, 'Religi'),
(2, 'Belanja'),
(3, 'Alam'),
(4, 'Kuliner'),
(5, 'Pendidikan'),
(6, 'Sejarah'),
(7, 'Budaya');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `user_address` varchar(40) NOT NULL,
  `user_phone_number` varchar(15) NOT NULL,
  `user_name` varchar(15) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `name`, `user_address`, `user_phone_number`, `user_name`, `password`) VALUES
(1, 'Bima', 'Malang', '085555555555', 'admin', '21232f297a57a5a743894a0e4a801fc3');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`event_id`);

--
-- Indexes for table `facility_tempat_wisata`
--
ALTER TABLE `facility_tempat_wisata`
  ADD PRIMARY KEY (`ta_facility`);

--
-- Indexes for table `hotel`
--
ALTER TABLE `hotel`
  ADD PRIMARY KEY (`hotel_id`);

--
-- Indexes for table `hotel_image`
--
ALTER TABLE `hotel_image`
  ADD PRIMARY KEY (`hotel_image_id`),
  ADD KEY `hotel_id` (`hotel_id`);

--
-- Indexes for table `kamar_hotel`
--
ALTER TABLE `kamar_hotel`
  ADD PRIMARY KEY (`id_kamar_hotel`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id_slider`);

--
-- Indexes for table `ta_facility`
--
ALTER TABLE `ta_facility`
  ADD PRIMARY KEY (`facility_id`),
  ADD KEY `ta_id` (`ta_id`),
  ADD KEY `ta_facility` (`ta_facility`);

--
-- Indexes for table `ta_image`
--
ALTER TABLE `ta_image`
  ADD PRIMARY KEY (`image_id`),
  ADD KEY `ta_id` (`ta_id`);

--
-- Indexes for table `ta_type`
--
ALTER TABLE `ta_type`
  ADD PRIMARY KEY (`ta_type_id`),
  ADD KEY `ta_id` (`ta_id`),
  ADD KEY `type_id` (`type_id`);

--
-- Indexes for table `tourist_atraction`
--
ALTER TABLE `tourist_atraction`
  ADD PRIMARY KEY (`ta_id`);

--
-- Indexes for table `type_tempat_wisata`
--
ALTER TABLE `type_tempat_wisata`
  ADD PRIMARY KEY (`id_type`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `event_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `facility_tempat_wisata`
--
ALTER TABLE `facility_tempat_wisata`
  MODIFY `ta_facility` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `hotel`
--
ALTER TABLE `hotel`
  MODIFY `hotel_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `hotel_image`
--
ALTER TABLE `hotel_image`
  MODIFY `hotel_image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `kamar_hotel`
--
ALTER TABLE `kamar_hotel`
  MODIFY `id_kamar_hotel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id_slider` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ta_facility`
--
ALTER TABLE `ta_facility`
  MODIFY `facility_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `ta_image`
--
ALTER TABLE `ta_image`
  MODIFY `image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ta_type`
--
ALTER TABLE `ta_type`
  MODIFY `ta_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tourist_atraction`
--
ALTER TABLE `tourist_atraction`
  MODIFY `ta_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `type_tempat_wisata`
--
ALTER TABLE `type_tempat_wisata`
  MODIFY `id_type` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
